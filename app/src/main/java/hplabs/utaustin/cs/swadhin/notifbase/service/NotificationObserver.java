package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.widget.RemoteViews;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import hplabs.utaustin.cs.swadhin.notifbase.NotifBaseMainActivity;

/**
 * This component observes the various notifications received and cleared by the user. The
 * notification events are logged along with the posting/clearing time and the package/app that
 * posted the notification.
 *
 * @author Abhinav Parate
 *
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationObserver extends NotificationListenerService {

    //public static ArrayList<StatusBarNotification> notifications = new ArrayList<StatusBarNotification>();
    //public static HashMap<String,ArrayList<StatusBarNotification>> nmaps = new HashMap<String,ArrayList<StatusBarNotification>>();
    //public static ArrayList<Integer> clearedList = new ArrayList<Integer>();
    private static NotificationObserver context = null;
    private volatile Boolean sensor_flag = true;

    public static int notification_action = 0;
    /*
    public static boolean isRecording = false;
    private Boolean audio_flag = true;
    private volatile Boolean audio_flag_log = true;
    public static String audio_feature_str = "spl-pitch-rms-mfcc:";
    */
    public static final String LOG_TAG = "Notifbase_Notif_Info";

    public static int notification_count = 0;
    public static int notification_clear = 0;
    public static String active_notif_arr ="";

    //private static final int NOTIFICATION_ID = 0x2376;


    @Override
    public int onStartCommand(Intent intent,int flags, int startId){
        super.onStartCommand(intent, flags, startId);
        context = this;
        return START_STICKY;
    }

    /* (non-Javadoc)
     * @see android.service.notification.NotificationListenerService#onNotificationPosted(android.service.notification.StatusBarNotification)
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onNotificationPosted(StatusBarNotification notification) {

        Log.d("III", "onnot");
        notification_count += 1;

        StatusBarNotification[] act_arr = getActiveNotifications();

        active_notif_arr ="";
        //Populating Active Notifications
        for(StatusBarNotification notif:act_arr){

            Notification not_new = notif.getNotification();
            active_notif_arr += notif.getTag() + ";" + not_new.tickerText + ";" +  Integer.toString(notif.getId()) + ";"
            + notif.getPackageName() +";" + notif.getClass().toString() + "|";

        }

        //Sensor Data Recording
        final Intent intent = new Intent(getApplicationContext(), SensorService.class);
        //Recording Sensor Data
        if(sensor_flag == true) {
            startService(intent);
            sensor_flag = false;
        }

        new Timer(true).schedule(new TimerTask() {
            public void run() {

                stopService(intent);

                if (isMyServiceRunning(SensorService.class) == true) {

                    Log.d("Observer", "Alive");
                    stopService(intent);

                }

            }
        }, 20000); //10 seconds of Sensor Data Recording

        new Timer(true).schedule(new TimerTask() {
            public void run() {
                sensor_flag = true;
            }
        }, 900000); //Wait for 15 minute at least before starting Sensor reading

        context = this;
        AppUsageEvent aue = new AppUsageEvent();
        aue.taskID = notification.getId();
        aue.packageName = notification.getPackageName();
        aue.className = notification.getClass().toString();
        //aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_ON;

        aue.eventtype = "event_notification";
        aue.eventtype_info = AppUsageEvent.EVENT_UNDEFINED_INFO;

        //aue.starttime = notification.getPostTime(); //Time when it was posted
        aue.starttime = System.currentTimeMillis(); //Time when it was posted
        aue.runtime = 0; // since we don't know when it ends
        aue.longitude = LocationObserver.longitude;
        aue.latitude = LocationObserver.latitude;
        aue.accuracy = LocationObserver.accuracy;
        aue.powerstate = HardwareObserver.powerstate;
        aue.wifistate = HardwareObserver.wifistate;
        aue.bluetoothstate = HardwareObserver.bluetoothstate;
        aue.headphones = HardwareObserver.headphones;
        aue.orientation = HardwareObserver.orientation;
        aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;
        String temp_str = "spl-pitch-rms-mfcc:";
        aue.service_info = NotifBaseMainActivity.service_info;

        try {
            aue.audio_info = new String(Compressor.compress(temp_str.getBytes( "ISO-8859-1" )), "ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }

        //aue.audio_info = NotificationObserver.audio_feature_str;
        aue.calendar_event_str = CalenderObserver.current_event_list;
        //aue.access_info = "None";

        Notification not = notification.getNotification();

        List<String> str_list = getText(not);
        String listString = "";
        String encryptedString = "!!$!!";
        int not_word_count = 0;

        for (String s : str_list)
        {
            listString += s + " ";

            try {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                messageDigest.update(s.getBytes("ISO-8859-1"));
                byte[] bt_arr = messageDigest.digest();
                encryptedString += new String(bt_arr,"ISO-8859-1") + "$@$";

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            not_word_count += 1;
        }


        int style = extractStyle(not.extras.getString(Notification.EXTRA_TEMPLATE));

        //Log.d("XXX", "id = " + notification.getId() + " Package Name" + notification.getPackageName() +
          //      " Post time = " + notification.getPostTime() + " Tag = " + notification.getTag() + " Text = " + not.toString() + "Style =" + Integer.toString(style));


        aue.notif_info = encryptedString.trim() + "!!$!!|" + notification.getTag() + "|"
                + not.tickerText + "|" + notification.getId() + "|" + Integer.toString(not.ledARGB) +"|"
                + Integer.toString(not.ledOffMS) + "|" + Integer.toString(not.ledOnMS) + "|"
                + not.sound + "|" + not.vibrate + "|" + Integer.toString(not.priority) + "|"
                + Integer.toString(not.icon) +"|" + Integer.toString(not_word_count) + "|" +
                notification.isClearable() + "|" + notification.isOngoing() + "|" + Integer.toString(style);


        //notifications.add(notification);
        //updateMap(notification);
        //AppSensorService.logEvent(aue);
        //To be Changed

        //if(notification.getId()!=NOTIFICATION_ID)
          //  updateNotification();

        //Audio Data Recording
        /*
        if( (isRecording == false) && (audio_flag == true)) {

            isRecording = true;
            audio_flag = false;
            audio_flag_log = false;
            AudioObserver.mfcc_list.clear();
            AudioObserver.spl_list.clear();
            AudioObserver.pitch_list.clear();
            AudioObserver.rms_list.clear();

            final AudioObserver audObserver = new AudioObserver();

            AudioDispatcher dispat = AudioDispatcherFactory.fromDefaultMicrophone(AudioObserver.sample_rate,
                    AudioObserver.audio_buffer_size, AudioObserver.audio_buffer_overlap);
            //AudioDispatcher dispat = AudioDispatcherFactory.defaultMicrophoneAudioDispatcher(AudioObserver.audio_buffer_size, AudioObserver.audio_buffer_overlap);

            if (null != dispat) {

                audObserver.setAudioDispatcher(dispat);
                audObserver.init();

                //audObserver.runAudioObserver();

                //long cur_time = System.currentTimeMillis();

                final Thread ath = new Thread(audObserver.getAudioDispatcher(), "Audio Dispatcher");
                ath.start();

                new Timer(true).schedule(new TimerTask() {
                    public void run() {

                        ath.interrupt();
                        if (ath.isAlive()) {
                            Log.d(LOG_TAG, "Alive");
                        }

                        audObserver.getAudioDispatcher().stop();
                        audObserver.setAudioDispatcher(null);

                        String tot_str = "spl-pitch-rms-mfcc:";

                        for (String s : AudioObserver.spl_list) {
                            tot_str += s + "|";
                        }

                        tot_str += ":";

                        for (String s : AudioObserver.pitch_list) {
                            tot_str += s + "|";
                        }

                        tot_str += ":";

                        for (String s : AudioObserver.rms_list) {
                            tot_str += s + "|";
                        }

                        tot_str += ":";
                        for (String s : AudioObserver.mfcc_list) {
                            tot_str += s + "|";
                        }


                        Log.i("HHH", tot_str);
                        audio_feature_str = tot_str;
                        isRecording = false;

                    }
                }, 5000); //10 second audio feature after notification posting
            }
        }

        new Timer(true).schedule(new TimerTask() {
            public void run() {
                audio_flag = true;
                audio_flag_log = true;
            }
        }, 900000); //Wait for 15 minute at least before starting SensorService again


        if( audio_flag_log == true) {
            try {
                aue.audio_info = new String(Compressor.compress(NotificationObserver.audio_feature_str.getBytes("ISO-8859-1")), "ISO-8859-1");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        */

        try {
            aue.audio_info = new String(Compressor.compress(NotifAccessService.audio_feature_str.getBytes("ISO-8859-1")), "ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }
        aue.calendar_event_str = CalenderObserver.current_event_list;
        aue.service_info = NotifBaseMainActivity.service_info;

        try {
            aue.sensor_info = new String(Compressor.compress("None".getBytes("ISO-8859-1")), "ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }

        AppSensorService.logEvent(aue);

        /*
        writeToFile(aue.toString());
        String str = readFromFile();
        List<String> list_str = new ArrayList<String>(Arrays.asList(str.trim().split(",")));
        int count = 0;

        String comp_str = "";
        for(String st: list_str){

            if(count == 17 || count ==20){
                try {
                    comp_str += Compressor.uncompress(st.getBytes("ISO-8859-1")) + "||";
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DataFormatException e) {
                    e.printStackTrace();
                }
            }else{
                comp_str += st + Integer.toString(count) +"||";
            }
            count++;
        }

        Log.d("TEST", comp_str);*/


        //Log.d("onNot", audio_feature_str);
        /*
        try {
            Log.d("onNot", Compressor.uncompress(aue.audio_info.getBytes("ISO-8859-1")));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataFormatException e) {
            e.printStackTrace();
        }*/


    }

    private String getEventText(AccessibilityEvent event) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : event.getText()) {
            sb.append(s);
            sb.append(" ");
        }
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see android.service.notification.NotificationListenerService#onNotificationRemoved(android.service.notification.StatusBarNotification)
     */
    @Override
    public void onNotificationRemoved(StatusBarNotification notification) {

        //Log.d("III", "onnotremove");
        notification_clear += 1;

       // context = this;
        AppUsageEvent aue = new AppUsageEvent();
        aue.taskID = notification.getId();
        aue.packageName = notification.getPackageName();
        aue.className = notification.getClass().toString();

        aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_OFF;
        aue.eventtype_info = AppUsageEvent.EVENT_UNDEFINED_INFO;

        aue.starttime = System.currentTimeMillis(); //Time when it was cleared
        aue.runtime = aue.starttime-notification.getPostTime(); // since we just cleared it.
        aue.longitude = LocationObserver.longitude;
        aue.latitude = LocationObserver.latitude;
        aue.accuracy = LocationObserver.accuracy;
        aue.powerstate = HardwareObserver.powerstate;
        aue.wifistate = HardwareObserver.wifistate;
        aue.bluetoothstate = HardwareObserver.bluetoothstate;
        aue.headphones = HardwareObserver.headphones;
        aue.orientation = HardwareObserver.orientation;
        aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;
        aue.service_info = NotifBaseMainActivity.service_info;
        String temp_str = "spl-pitch-rms-mfcc:";
        try {
            aue.audio_info = new String(Compressor.compress(temp_str.getBytes( "ISO-8859-1" )), "ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }
        aue.calendar_event_str = CalenderObserver.current_event_list;
        aue.access_info = "|!!$!!None";

        Notification not = notification.getNotification();

        String listString = "None";
        String encryptedString = "!!$!!None$@$!!$!!";

        //Log.d("onNot", "id = " + notification.getId() + " Package Name" + notification.getPackageName() +
          //      " Post time = " + notification.getPostTime() + " Tag = " + notification.getTag() + " Text = " + listString);

        aue.notif_info = encryptedString.trim() + "|" + notification.getTag() + "|"
                + not.tickerText + "|" + not.icon + "|" + Integer.toString(not.ledARGB) + "|"
                + Integer.toString(not.ledOffMS) + "|" + Integer.toString(not.ledOnMS) + "|"
                + not.sound + "|" + not.vibrate + "|" + listString + "|" + notification.isClearable() + "|"
                + notification.isOngoing() + "|" + notification.getId() + "|" + Integer.toString(not.priority);


        /*if(notifications.size()>20)
        {
            for(StatusBarNotification iNotification: notifications)
            {
                if(iNotification.getId() == notification.getId())
                {
                    notifications.remove(iNotification);
                    break;
                }
            }
        }*/

        //removeFromMap(notification);
        AppSensorService.logEvent(aue);
    /*
        writeToFile(aue.toString());
        String str = readFromFile();
        List<String> list_str = new ArrayList<String>(Arrays.asList(str.trim().split(",")));
        int count = 0;

        String comp_str = "";
        for(String st: list_str){

            if(count == 18 || count ==20){
                try {
                    comp_str += Compressor.uncompress(st.getBytes("ISO-8859-1")) + "||";
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DataFormatException e) {
                    e.printStackTrace();
                }
            }else{
                comp_str += st + "||";
            }
            count++;
        }

        Log.d("TEST", comp_str);*/

    }

/*
    private static void updateMap(StatusBarNotification notification)
    {
        String app = notification.getPackageName();
        ArrayList<StatusBarNotification> list = getNotificationList(app);
        int id = notification.getId();
        for(StatusBarNotification sn: list)
        {
            if(sn.getId()==id)
            {
                list.remove(sn);
            }
        }
        list.add(notification);

    }

    private static void removeFromMap(StatusBarNotification notification)
    {
        String app = notification.getPackageName();
        int id = notification.getId();
        for(Integer cId: clearedList)
        {
            if(cId==id)
            {
                //This notification was cleared by us
                //Keep it in memory
                clearedList.remove(id);
                return;
            }
        }
        ArrayList<StatusBarNotification> list = getNotificationList(app);
        for(StatusBarNotification sn: list)
        {
            if(sn.getId()==id)
            {
                list.remove(sn);
                break;
            }
        }
    }

    private static ArrayList<StatusBarNotification> getNotificationList(String app)
    {
        ArrayList<StatusBarNotification> list = nmaps.get(app);
        if(list==null)
        {
            list = new ArrayList<StatusBarNotification>();
            nmaps.put(app, list);
        }
        return list;
    }
*/
    public static NotificationObserver getInstance()
    {
        return context;
    }

    public static List<String> getText(Notification notification)
    {
        // We have to extract the information from the view
        RemoteViews views = (RemoteViews)notification.bigContentView;
        if (views == null) views = (RemoteViews)notification.contentView;
        if (views == null) views = (RemoteViews)notification.tickerView;
        if (views == null) return null;

        // Use reflection to examine the m_actions member of the given RemoteViews object.
        // It's not pretty, but it works.
        List<String> text = new ArrayList<String>();
        try
        {
            Field field = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //Log.e("Obs", views.getClass().getSuperclass().getSimpleName());
                field = views.getClass().getSuperclass().getDeclaredField("mActions");
            }else{
                //Log.e("Obs", views.getClass().getSimpleName());
                field = views.getClass().getDeclaredField("mActions");
            }

            field.setAccessible(true);

            @SuppressWarnings("unchecked")
            ArrayList<Parcelable> actions = (ArrayList<Parcelable>) field.get(views);

            // Find the setText() and setTime() reflection actions
            for (Parcelable p : actions)
            {
                Parcel parcel = Parcel.obtain();

                if (parcel == null) continue;
                if (p == null) continue;

                p.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);

                // The tag tells which type of action it is (2 is ReflectionAction, from the source)
                int tag = parcel.readInt();
                if (tag != 2) continue;

                // View ID
                parcel.readInt();

                String methodName = parcel.readString();
                if (methodName != null)
                    Log.e("Obs", methodName);

                if (methodName == null) continue;
                    // Save strings
                else if (methodName.equals("setText"))
                {
                    // Parameter type (10 = Character Sequence)
                    parcel.readInt();

                    // Store the actual string
                    if( null != parcel) {
                        CharSequence cs = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
                        if (null != cs) {

                            String t = cs.toString();

                            if (null != t) {
                                text.add(t.trim());
                            }
                        }
                    }
                }

                // Save times. Comment this section out if the notification time isn't important
                else if (methodName.equals("setTime"))
                {
                    // Parameter type (5 = Long)
                    parcel.readInt();

                    if( null != parcel) {
                        String t = new SimpleDateFormat("h:mm a").format(new Date(parcel.readLong()));
                        text.add(t);
                    }
                }

                parcel.recycle();

            }
        }

        // It's not usually good style to do this, but then again, neither is the use of reflection...
        catch (Exception e)
        {
            Log.e("NotificationObserver", e.toString());
        }

        return text;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private int extractStyle(String styleString)
    {
        int style = -1;
        if(styleString!=null)
        {
            if(styleString.equals("android.app.Notification$BigTextStyle"))
            {
                style = 0;
            } else if(styleString.equals("android.app.Notification$BigPictureStyle"))
            {
                style = 1;
            } else if(styleString.equals("android.app.Notification$InboxStyle"))
            {
                style = 2;
            } else if(styleString.equals("android.app.Notification$MediaStyle"))
            {
                style = 3;
            } else
            {
                style = -1;
            }
        } else {
            style = -1;
        }
        return style;
    }

    public void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config_new.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("config_new.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }


}


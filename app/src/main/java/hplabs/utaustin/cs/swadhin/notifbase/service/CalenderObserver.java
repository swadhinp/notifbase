package hplabs.utaustin.cs.swadhin.notifbase.service;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class CalenderObserver{

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static String current_event_list = "None";

    // Default constructor
    public static void readCalendar(Context context) {
        readCalendar(context, 0, 10);
    }

    // Use to specify specific the time span
    public static void readCalendar(Context context, int days, int hours) {

        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = null;

        // Create a cursor and read from the calendar (for Android API below 4.0)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"),
                    (new String[]{"_id", "displayName", "selected"}), null, null, null);
        } else {

            //cursor = contentResolver.query(Uri.parse("content://com.android.calendar/events"),
               // new String[]{"calendar_id", "title", "description", "dtstart", "dtend", "eventLocation"},
              //  null, null, null);

            String[] projection =
                    new String[]{
                            CalendarContract.Calendars._ID,
                            CalendarContract.Calendars.NAME,
                            CalendarContract.Calendars.ACCOUNT_NAME,
                            CalendarContract.Calendars.ACCOUNT_TYPE};

            cursor = context.getContentResolver().
                            query(CalendarContract.Calendars.CONTENT_URI,
                                    projection,
                                    CalendarContract.Calendars.VISIBLE + " = 1",
                                    null,
                                    CalendarContract.Calendars._ID + " ASC");
        }

        // Create a set containing all of the calendar IDs available on the phone
        HashSet<String> calendarIds = getCalenderIds(cursor);

        // Create a hash map of calendar ids and the events of each id
        HashMap<String, List<CalendarEvent>> eventMap = new HashMap<String, List<CalendarEvent>>();

        // Loop over all of the calendars
        for (String id : calendarIds) {

            // Create a builder to define the time span
            //Uri.Builder builder = null;

            //if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Uri.Builder builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
            //}

            long now = new Date().getTime();
            long start_time = now - (DateUtils.DAY_IN_MILLIS * days) - (DateUtils.HOUR_IN_MILLIS * hours);
            //long start_time = now - (DateUtils.DAY_IN_MILLIS * days) - (DateUtils.HOUR_IN_MILLIS * hours);
            //long end_time = now + (DateUtils.DAY_IN_MILLIS * days) + (DateUtils.HOUR_IN_MILLIS * hours);
            long end_time = now;

            // create the time span based on the inputs
            ContentUris.appendId(builder, now - (DateUtils.HOUR_IN_MILLIS * hours) );
            ContentUris.appendId(builder, now );

            // Create an event cursor to find all events in the calendar
            Cursor eventCursor = null;
            Log.d("YYY", "Id =" + id);

            try {

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {

                    eventCursor = contentResolver.query(builder.build(),
                            new String[]{"title", "begin", "end", "allDay"}, "Calendars._id=" + id,
                            null, "startDay ASC, startMinute ASC");
                }else{
                    eventCursor = context.getContentResolver()
                            .query(
                                    Uri.parse("content://com.android.calendar/events"),
                                    new String[] {"title", "description",
                                            "dtstart", "dtend", "eventLocation"}, "((calendar_id=" + id + ") AND ((dtstart >= "+ Long.toString(start_time) +") AND (dtend <= "+Long.toString(end_time)+")))",
                                    null, null);
                }

            }catch(Exception e){
                Log.d("YYY", e.toString());
                continue;

            }

            Log.d("YYY", "Id =" + id + "eventCursor count="+ Integer.toString(eventCursor.getCount()));

            // If there are actual events in the current calendar, the count will exceed zero
            if(eventCursor.getCount()>0)
            {

                // Create a list of calendar events for the specific calendar
                List<CalendarEvent> eventList = new ArrayList<CalendarEvent>();

                // Move to the first object
                eventCursor.moveToFirst();

                // Create an object of CalendarEvent which contains the title, when the event begins and ends,
                // and if it is a full day event or not
                CalendarEvent ce = loadEvent(eventCursor);

                // Adds the first object to the list of events
                eventList.add(ce);
                //current_event_list = ce.toString();

                //System.out.println(ce.toString());

                // While there are more events in the current calendar, move to the next instance

                while (eventCursor.moveToNext())
                {

                    // Adds the object to the list of events
                    ce = loadEvent(eventCursor);
                    eventList.add(ce);

                    //current_event_list += "==" + ce.toString();
                    //System.out.println(ce.toString());

                }

                Collections.sort(eventList);
                eventMap.put(id, eventList);

                Log.d("YYYY", Integer.toString(eventMap.keySet().size()) + " " + eventMap.values());

            }
        }

        int count = 0;

        for( List<CalendarEvent> cev: eventMap.values()){

            for(CalendarEvent cv : cev){

                if(count == 0){
                    current_event_list = cv.toString();
                    count += 1;
                }else{
                    current_event_list += "==" + cv.toString();
                }
            }
        }
    }

    // Returns a new instance of the calendar object
    private static CalendarEvent loadEvent(Cursor csr) {
        return new CalendarEvent(csr.getString(0),
                getDate(csr.getLong(1)),
                getDate(csr.getLong(2)));
        //,
              //  !csr.getString(3).equals("0"));
    }

    public static String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    // Creates the list of calendar ids and returns it in a set
    private static HashSet<String> getCalenderIds(Cursor cursor) {

        HashSet<String> calendarIds = new HashSet<String>();

        try
        {

            // If there are more than 0 calendars, continue
            if(cursor.getCount() > 0)
            {

                // Loop to set the id for all of the calendars
                while (cursor.moveToNext()) {

                    String _id = cursor.getString(0);
                    String displayName = cursor.getString(1);
                    //Boolean selected = !cursor.getString(2).equals("0");
                    //Getting all calendars only associated with email ids
                    if( null != displayName) {
                        if (validate(displayName)) {

                            Log.d("YYY", "Id: " + _id + " Display Name: " + displayName);
                            calendarIds.add(_id);
                        }
                    }

                }
            }
        }

        catch(AssertionError ex)
        {
            ex.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return calendarIds;

    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }
}
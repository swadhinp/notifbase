package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import hplabs.utaustin.cs.swadhin.notifbase.NotifBaseMainActivity;

/**
 * An abstract service that must be extended by each sensor supported
 * by the Notifbase App
 *
 * @author Swadhin
 *
 */
public class SensorService extends Service implements SensorEventListener {


    private static final String DEBUG_TAG = "SensorLoggerService";
    private SensorManager mSensorManager;
    private List<Sensor> deviceSensors;
    private int numSensors = 0;
    private boolean[] avlSensors = new boolean[11];
    
    //Different Sensors
    private Sensor mAcc = null;
    private Sensor mMag = null;
    private Sensor mGyro = null;
    private Sensor mProxy = null;
    private Sensor mLight = null;
    private Sensor mRotv = null;
    private Sensor mTemp = null;
    private Sensor mPressure = null;
    private Sensor mLacc = null;
    private Sensor mRel = null;
    private Sensor mStep = null;

    public static String light_value = "10 lx";
    public static String acc_value = "0:0:0 m/s^2";
    public static String prox_value = "5 Cm";

    /**
     * Class used for the client Binder. For interaction with service
     * without IPC
     */
    public class SensorBinder extends Binder {
        public SensorService getService() {
            // Return this instance of LocalService so clients can call public methods
            return SensorService.this;
        }
    }


    private final IBinder mBinder = new SensorBinder();


    /* (non-Javadoc)
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        stopSelf();
        super.onDestroy();

        for(int i =0 ; i< numSensors ; i++)
        {
            mSensorManager.unregisterListener(this);
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        numSensors = deviceSensors.size();
        Arrays.fill(avlSensors, Boolean.FALSE);

        for (int i =0 ; i < numSensors; i++)
        {
            int pos = numTypeSensor(deviceSensors.get(i).getType());
            // Success! There's a sensor
            if ( pos != -1)
            {
                avlSensors[pos] = Boolean.TRUE;

            }else {
                // Failure! No sensor.
                //avlSensors[pos] = -1;····
            }
        }

        populateAndRegisterSensors();

        return START_NOT_STICKY; // run until explicitly stopped.
    }


    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        // grab the values and timestamp
        //...
        // stop the sensor and service

        float[] senData = null;
        senData = event.values;
        String sen_feature = new String();

        switch( event.sensor.getType() ) {

            case Sensor.TYPE_ACCELEROMETER :
                //Log.e("HERE : Acc", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_ACCELEROMETER);
                acc_value = sen_feature + sensorUnit(Sensor.TYPE_ACCELEROMETER);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD :
                //Log.e("HERE : MF", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_MAGNETIC_FIELD);
                break;
            case Sensor.TYPE_GYROSCOPE :
                //Log.e("HERE : GE", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_GYROSCOPE);
                break;
            case Sensor.TYPE_PROXIMITY :
                //Log.e("HERE : PX", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_PROXIMITY);
                prox_value = sen_feature + sensorUnit(Sensor.TYPE_PROXIMITY);
                break;
            case Sensor.TYPE_LIGHT :
                //Log.e("HERE : LT", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_LIGHT);
                light_value = sen_feature + sensorUnit(Sensor.TYPE_LIGHT);
                break;
            case Sensor.TYPE_ROTATION_VECTOR :
                //Log.e("HERE : RV", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_ROTATION_VECTOR);
                break;
            case Sensor.TYPE_AMBIENT_TEMPERATURE :
                //Log.e("HERE : AT", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_AMBIENT_TEMPERATURE);
                break;
            case Sensor.TYPE_PRESSURE :
                //Log.e("HERE : Pr", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_PRESSURE);
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION :
                //Log.e("HERE : LAcc", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_LINEAR_ACCELERATION);
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY :
                //Log.e("HERE : Rl", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_RELATIVE_HUMIDITY);
                break;
            case Sensor.TYPE_STEP_COUNTER :
                //Log.e("HERE : ST", Integer.toString(event.sensor.getType()) );
                sen_feature = appendAndSend(senData, Sensor.TYPE_STEP_COUNTER);
                break;
            default:
                //Do Nothing
                break;

        }

        logSensorEvent(sen_feature, event.sensor.getType());
        Log.d(DEBUG_TAG, sen_feature);


        final SensorEventListener sen = this;

        new Timer(true).schedule(new TimerTask() {
            public void run() {

                mSensorManager.unregisterListener(sen);
                stopSelf();
            }
        }, 5000);

    }

    private void populateAndRegisterSensors()
    {
        if(avlSensors[numTypeSensor(Sensor.TYPE_ACCELEROMETER)] == Boolean.TRUE) {
            mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_MAGNETIC_FIELD)] == Boolean.TRUE) {
            mMag = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mSensorManager.registerListener(this, mMag, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_GYROSCOPE)] == Boolean.TRUE) {
            mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_PROXIMITY)] == Boolean.TRUE) {
            mProxy = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
            mSensorManager.registerListener(this, mProxy, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_LIGHT)] == Boolean.TRUE) {
            mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
            mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_ROTATION_VECTOR)] == Boolean.TRUE) {
            mRotv = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            mSensorManager.registerListener(this, mRotv, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)] == Boolean.TRUE) {
            mTemp = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            mSensorManager.registerListener(this, mTemp, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_PRESSURE)] == Boolean.TRUE) {
            mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
            mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_LINEAR_ACCELERATION)] == Boolean.TRUE) {
            mLacc = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
            mSensorManager.registerListener(this, mLacc, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_RELATIVE_HUMIDITY)] == Boolean.TRUE) {
            mRel = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
            mSensorManager.registerListener(this, mRel, SensorManager.SENSOR_DELAY_UI);
        }

        if(avlSensors[numTypeSensor(Sensor.TYPE_STEP_COUNTER)] == Boolean.TRUE) {
            mStep = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
            mSensorManager.registerListener(this, mStep, SensorManager.SENSOR_DELAY_UI);
        }

    }

    private String appendAndSend(float[] senData,int type)
    {
        String showData = "";
        StringBuilder bTemp = new StringBuilder();
        DecimalFormat df = new DecimalFormat("##.#####");

        if( null != senData )
        {
            for( int j =0 ; j< numOfViews(type); j++)
            {
                if( ( type == Sensor.TYPE_GYROSCOPE))
                {
                    showData = df.format(senData[j]);
                }
                else
                {
                    showData = ""+senData[j];
                }
                //bTemp.append(showData+sensorUnit(type)+",");

                if( j== 0) {
                    bTemp.append(showData);

                }else{

                    bTemp.append(":" + showData);

                }

            }
        }

        return bTemp.toString();
    }

    private void logSensorEvent( String feat, int type ){


        AppUsageEvent aue = new AppUsageEvent();
        aue.taskID = 0; //windowid
        aue.packageName = "None";
        aue.className = "None";

        //aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_ON;

        aue.eventtype = AppUsageEvent.EVENT_SENSOR;
        aue.eventtype_info = getSensorName(type); //getSensorName(numTypeofSensor(type)) wrong code

        Log.e("HERE", getSensorName(type) + " : " + feat);
        aue.starttime = System.currentTimeMillis(); //Time when it was posted
        aue.runtime = 0; // since we don't know when it ends
        aue.longitude = LocationObserver.longitude;
        aue.latitude = LocationObserver.latitude;
        aue.accuracy = LocationObserver.accuracy;
        aue.powerstate = HardwareObserver.powerstate;
        aue.wifistate = HardwareObserver.wifistate;
        aue.bluetoothstate = HardwareObserver.bluetoothstate;
        aue.headphones = HardwareObserver.headphones;
        aue.orientation = HardwareObserver.orientation;
        aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;
        String temp_str = "spl-pitch-rms-mfcc:";

        try {
            aue.audio_info = new String(Compressor.compress(temp_str.getBytes( "ISO-8859-1" )), "ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }

        aue.calendar_event_str = CalenderObserver.current_event_list;
        aue.notif_info = "!!$!!None$@$!!$!!|";
        aue.access_info = "|!!$!!None";
        aue.service_info = NotifBaseMainActivity.service_info;


        try {
            aue.sensor_info = new String(Compressor.compress(feat.getBytes( "ISO-8859-1" )), "ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }

        //aue.sensor_info = feat;

        AppSensorService.logEvent(aue);

        //Testing code for writing and reading

        /*
        writeToFile(aue.toString());
        String str = readFromFile();
        List<String> list_str = new ArrayList<String>(Arrays.asList(str.trim().split(",")));
        int count = 0;

        String comp_str = "";
        for(String st: list_str){

            if(count == 18){
                try {
                    comp_str += Compressor.uncompress(st.getBytes("ISO-8859-1")) + "||";
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DataFormatException e) {
                    e.printStackTrace();
                }
            }else{
                comp_str += st  +"||";
            }
            count++;
        }

        Log.d("TEST", comp_str);
        */

    }

    private int numOfViews (int type)
    {
        int num = 0;
        switch(type)
        {
            case Sensor.TYPE_ACCELEROMETER :
            case Sensor.TYPE_MAGNETIC_FIELD :
            case Sensor.TYPE_GYROSCOPE :
            case Sensor.TYPE_ROTATION_VECTOR :
            case Sensor.TYPE_LINEAR_ACCELERATION :
                num = 3;
                break;
            case Sensor.TYPE_LIGHT :
            case Sensor.TYPE_AMBIENT_TEMPERATURE :
            case Sensor.TYPE_PRESSURE :
            case Sensor.TYPE_RELATIVE_HUMIDITY:
            case Sensor.TYPE_STEP_COUNTER:
            case Sensor.TYPE_PROXIMITY:
                num = 1;
                break;
            default:
                num = -1;
                break;
        }

        return num;

    }
    private int numTypeSensor(int type)
    {
        int result = -1;


        switch(type)
        {
            case Sensor.TYPE_ACCELEROMETER :
                result = 0;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD :
                result = 1;
                break;
            case Sensor.TYPE_GYROSCOPE :
                result = 2;
                break;
            case Sensor.TYPE_PROXIMITY :
                result = 3;
                break;
            case Sensor.TYPE_LIGHT :
                result = 4;
                break;
            case Sensor.TYPE_ROTATION_VECTOR :
                result = 5;
                break;
            case Sensor.TYPE_AMBIENT_TEMPERATURE :
                result = 6;
                break;
            case Sensor.TYPE_PRESSURE :
                result = 7;
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION :
                result = 8;
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY :
                result = 9;
                break;
            case Sensor.TYPE_STEP_COUNTER :
                result = 10;
                break;
            default:
                result = -1;
                break;
        }

        return result;

    }

    private int revNumTypeSensor(int type)
    {
        int result = -1;
        switch(type)
        {
            case 0 :
                result = Sensor.TYPE_ACCELEROMETER;
                break;
            case 1:
                result = Sensor.TYPE_MAGNETIC_FIELD ; //=>Accelerometer
                break;
            case 2:
                result = Sensor.TYPE_GYROSCOPE; //=>Magnetic Field
                break;
            case 3:
                result = Sensor.TYPE_PROXIMITY ;
                break;
            case  4:
                result = Sensor.TYPE_LIGHT; //=>Gyroscope
                break;
            case  5:
                result = Sensor.TYPE_ROTATION_VECTOR;
                break;
            case  6:
                result = Sensor.TYPE_AMBIENT_TEMPERATURE; //=>Pressure
                break;
            case  7:
                result = Sensor.TYPE_PRESSURE;
                break;
            case  8:
                result = Sensor.TYPE_LINEAR_ACCELERATION;
                break;
            case 9:
                result = Sensor.TYPE_RELATIVE_HUMIDITY;
                break;
            case 10 :
                result = Sensor.TYPE_STEP_COUNTER; //=> LAcc
                break;
            default:
                result = -1; //=>Rotation Vector
                break;
        }
        return result;
    }

    private String getSensorName(int type)
    {
        String result = "UNDEFINED";

        switch(type)
        {
            case 0 :
                result = "TYPE_ACCELEROMETER";
                break;
            case 1:
                result = "TYPE_MAGNETIC_FIELD";
                break;
            case 2:
                result = "TYPE_GYROSCOPE";
                break;
            case 3:
                result = "TYPE_PROXIMITY";
                break;
            case  4:
                result = "TYPE_LIGHT";
                break;
            case  5:
                result = "TYPE_ROTATION_VECTOR";
                break;
            case  6:
                result = "TYPE_AMBIENT_TEMPERATURE";
                break;
            case  7:
                result = "TYPE_PRESSURE";
                break;
            case  8:
                result = "TYPE_LINEAR_ACCELERATION";
                break;
            case 9:
                result = "TYPE_RELATIVE_HUMIDITY";
                break;
            case 10 :
                result = "TYPE_STEP_COUNTER";
                break;
            default:
                result = "UNDEFINED";
                break;
        }
        return result;
    }

    private String sensorUnit (int type) {
        String sUnit;
        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
            case Sensor.TYPE_LINEAR_ACCELERATION:
                sUnit = " m/s^2";
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                sUnit = " uT";
                break;
            case Sensor.TYPE_GYROSCOPE:
                sUnit = " rad/s";
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                sUnit = " ";
                break;
            case Sensor.TYPE_LIGHT:
                sUnit = " lx";
                break;
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
                sUnit = " Cent";
                break;
            case Sensor.TYPE_PRESSURE:
                sUnit = " mb";
                break;
            case Sensor.TYPE_PROXIMITY:
                sUnit = " Cm";
                break;
            case Sensor.TYPE_STEP_COUNTER:
                sUnit = " Sc";
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                sUnit = " Rh";
                break;
            default:
                sUnit = " ";
                break;
        }

        return sUnit;

    }

    public void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config_new.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("config_new.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }


}


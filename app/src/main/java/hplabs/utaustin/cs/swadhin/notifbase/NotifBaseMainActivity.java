package hplabs.utaustin.cs.swadhin.notifbase;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import hplabs.utaustin.cs.swadhin.notifbase.service.AppSensorService;
import hplabs.utaustin.cs.swadhin.notifbase.service.AudioObserver;
import hplabs.utaustin.cs.swadhin.notifbase.service.CalendarRecorder;
import hplabs.utaustin.cs.swadhin.notifbase.service.CalenderObserver;
import hplabs.utaustin.cs.swadhin.notifbase.service.HardwareObserver;
import hplabs.utaustin.cs.swadhin.notifbase.service.LocationObserver;
import hplabs.utaustin.cs.swadhin.notifbase.service.NotifAccessService;
import hplabs.utaustin.cs.swadhin.notifbase.service.NotificationObserver;
import hplabs.utaustin.cs.swadhin.notifbase.service.SensorService;
import hplabs.utaustin.cs.swadhin.notifbase.service.ServicePermissionChecker;


public class NotifBaseMainActivity extends ActionBarActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;
    SharedPreferences prefs = null;
    private AlertDialog alert_dialog = null;
    private Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            TimerTask task_acc = new ServicePermissionChecker(getApplicationContext());
            Timer timer_acc = new Timer();

            timer_acc.schedule(task_acc, 5000);

            handler.postDelayed(this, 300000); //Every 5 Min
        }
    };
    //public static boolean app_acc = false;
    //AudioObserver audObserver = null;
    CalenderObserver calObserver;

    //private int not_permission;
    //private int acc_permission;
    //private int app_permission;

    public volatile boolean not_flag = false;
    public volatile boolean app_flag = true;
    public volatile boolean acc_flag = false;
    public static String service_info = "0";

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_base_main);

        //Blocking Auto Orientation
        int current = getRequestedOrientation();
        // only switch the orientation if not in portrait
        if ( current != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT ) {
            setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );
        }

        /*
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Resources rcs = getResources();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(rcs.getString(R.string.pref_appsensor_active),true);
        editor.apply();
        AppSensorService.startByIntent(getBaseContext());
        */

        startService( new Intent(getApplicationContext(), NotificationObserver.class));
        //startService( new Intent(getApplicationContext(), AppSensorService.class));
        AppSensorService.startByIntent(getBaseContext());

        //Loading Alert Dialogs
        showAlertDialog();

        //Run Calendar Hourly
        TimerTask task = new CalendarRecorder(getApplicationContext());
        Timer timer = new Timer();

        // scheduling the calendar info reading task hourly
        timer.scheduleAtFixedRate(task, new Date(), 1800000);
        //Log.d("MAIN", CalenderObserver.current_event_list);


        //Run Accesibility Service Permission 5 minutes
        //TimerTask task_acc = new ServicePermissionChecker(getApplicationContext());
        //Timer timer_acc = new Timer();


        // scheduling the task
        //timer_acc.scheduleAtFixedRate(task_acc, new Date(), 300000);
        handler.postDelayed(runnable, 300000); //Every 5 Min
        //Log.d("MAIN", NotifBaseMainActivity.service_info);

        //newFragment.show(getFragmentManager(), "dialog");
        //alert_dialog = getAlertDialog(strArray, title, this);
        //alert_dialog.show();

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        //actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }

    private void showAlertDialog(){
        List<String> d_arr = checkAndPopulateDialog();
        String[] strArray = d_arr.toArray(new String[d_arr.size()]);

        String title = "Needed Permissions";

        //DialogFragment newFragment = PermissionAlertDialogFragment.newInstance(
          //      R.string.alert_dialog_permission, this, title, strArray);
        //newFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Panel);
        //newFragment.setCancelable(false);
        //newFragment.show(getFragmentManager(), "dialog");

        if( d_arr.size() > 0) {

            if( alert_dialog == null) {
                alert_dialog = getAlertDialog(strArray, title, this).create();
                alert_dialog.setCancelable(false);
                alert_dialog.show();
            }

        }
    }

    @Override
    protected void onPause() {

        super.onPause();
        List<String> d_arr = checkAndPopulateDialog();

        if( app_flag == true && not_flag == true && acc_flag == true) {

            if( alert_dialog != null) {
                alert_dialog.dismiss();
            }

        }

       // Toast.makeText(getApplicationContext(), "onpause " + Boolean.toString(app_flag) +
         //       ":" + Boolean.toString(not_flag) + ":" + Boolean.toString(acc_flag),Toast.LENGTH_SHORT).show();


        if( app_flag == false || not_flag == false || acc_flag == false){

            String[] strArray = d_arr.toArray(new String[d_arr.size()]);
            String title = "Needed Permissions";

            if( d_arr.size() > 0) {

                alert_dialog.dismiss();
                alert_dialog = getAlertDialog(strArray, title, this).create();
                alert_dialog.setCancelable(false);
                alert_dialog.show();
            }
        }

    }

    @Override
    protected void onResume() {


        super.onResume();
        List<String> d_arr = checkAndPopulateDialog();

        if( app_flag == true && not_flag == true && acc_flag == true) {

            if( alert_dialog != null) {
                alert_dialog.dismiss();
            }

        }

        //Toast.makeText(getApplicationContext(), "onresume " + Boolean.toString(app_flag) +
          //      ":" + Boolean.toString(not_flag) + ":" + Boolean.toString(acc_flag),Toast.LENGTH_SHORT).show();

        if( app_flag == false || not_flag == false || acc_flag == false) {
            String[] strArray = d_arr.toArray(new String[d_arr.size()]);
            String title = "Needed Permissions";

            if( d_arr.size() > 0) {
                alert_dialog.dismiss();
                alert_dialog = getAlertDialog(strArray, title, this).create();
                alert_dialog.setCancelable(false);
                alert_dialog.show();
            }
        }

    }

    private List<String> checkAndPopulateDialog() {


        PackageManager pm = getApplicationContext().getPackageManager();

        acc_flag = isAccessibilityEnabled(this, "hplabs.utaustin.cs.swadhin.notifbase/.service.NotifAccessService");

        if(acc_flag == true){
            service_info = "1";
        }else{
            service_info = "0";
        }

        //not_flag = hasPermission("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE");

        not_flag = isNotificationEnabled(this, this.getPackageName() );

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {

            //app_flag = hasPermission("android.permission.GET_TASKS");
            app_flag = isAppUsageEnabled(this);
            //app_flag = app_acc;
        }

        //Toast.makeText(this, "check " + Boolean.toString(app_flag) +
          //      ":" + Boolean.toString(not_flag) + ":" + Boolean.toString(acc_flag),Toast.LENGTH_SHORT).show();

        /*
        if(acc_permission == PackageManager.PERMISSION_GRANTED ) {
            acc_flag = true;
        }

        not_permission = pm.checkPermission(
                Manifest.permission.BIND_NOTIFICATION_LISTENER_SERVICE,
                getApplicationContext().getPackageName());

        if(not_permission == PackageManager.PERMISSION_GRANTED ) {
            not_flag = true;
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            app_permission = pm.checkPermission(
                    Manifest.permission.GET_TASKS,
                    getApplicationContext().getPackageName());

            if(app_permission == PackageManager.PERMISSION_GRANTED ) {
                app_flag = true;
            }

        }*/

        List<String> dial_arr = new ArrayList<>();

        if(not_flag == false){

            dial_arr.add("Please Give Notification Access");
        }

            if (acc_flag == false){

                dial_arr.add("Please Give Accessibility Access");
        }

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            if (app_flag == false) {
                dial_arr.add("Please Give Application Access");
            }
        }

        return dial_arr;
    }

    private boolean isAccessibilityEnabled(Context context, String id) {

        AccessibilityManager am = (AccessibilityManager) context
                .getSystemService(Context.ACCESSIBILITY_SERVICE);

        List<AccessibilityServiceInfo> runningServices = am
                .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
        for (AccessibilityServiceInfo service : runningServices) {
            if (id.equals(service.getId())) {
                return true;
            }
        }

        return false;
    }

    private boolean isNotificationEnabled(Context context, String packageName){
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");

// check to see if the enabledNotificationListeners String contains our package name
        if (enabledNotificationListeners == null || !enabledNotificationListeners.contains(packageName))
        {
            // in this situation we know that the user has not granted the app the Notification access permission
            return false;
        }
        else
        {
           return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private boolean isAppUsageEnabled( Context context){
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void logInstalledAccessiblityServices(Context context) {

        AccessibilityManager am = (AccessibilityManager) context
                .getSystemService(Context.ACCESSIBILITY_SERVICE);

        List<AccessibilityServiceInfo> runningServices = am
                .getInstalledAccessibilityServiceList();
        for (AccessibilityServiceInfo service : runningServices) {
            Log.i("Service", service.getId());
        }
    }


    public AlertDialog.Builder getAlertDialog(final String[] strArray,
                                                     String strTitle, final Activity activity) {

        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle(strTitle);

        alertDialogBuilder.setItems(strArray,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String str1 = strArray[which];
                        String str2 = "Notification";
                        String str3 = "Accessibility";
                        String str4 = "Application";

                        if( str1.toLowerCase().contains(str2.toLowerCase())) {

                            //Toast.makeText(activity, Integer.toString(which), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                            startActivity(intent);
                        }
                        else if( str1.toLowerCase().contains(str3.toLowerCase())){

                            //Toast.makeText(activity, Integer.toString(which), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                            startActivity(intent);

                        }
                        else if (str1.toLowerCase().contains(str4.toLowerCase())){

                            //Toast.makeText(activity, Integer.toString(which), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                            startActivity(intent);
                            app_flag = true;

                        }
                        //rest of your implementation
                    }
                });
        return alertDialogBuilder;
    }

    public boolean hasPermission(String permission)
    {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_PERMISSIONS);
            if (info.requestedPermissions != null) {
                for (String p : info.requestedPermissions) {
                    Log.d("Perm", p);
                    if (p.equals(permission)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notif_base_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       // if (id == R.id.action_settings) {
         //   return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());

        if (isMyServiceRunning(AppSensorService.class) == false){
            AppSensorService.startByIntent(getBaseContext());
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //Log.d("Tag","i = "+ position);

            switch (position) {
                case 0:
                    return NotifFragment.newInstance(position);
                case 1:
                    return InfoFragment.newInstance(position);
                case 2:
                    return AboutFragment.newInstance(position);
                default:
                    return PlaceholderFragment.newInstance(position + 1);
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_notif_base_main, container, false);
            TextView tv = (TextView)rootView.findViewById(R.id.section_label);
            tv.setText("Place: This is an App developed in HP Labs");
            return rootView;
        }
    }

    public static class NotifFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static NotifFragment newInstance(int sectionNumber) {
            NotifFragment fragment = new NotifFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public NotifFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_notif_base_main, container, false);

            final TextView tv = (TextView)rootView.findViewById(R.id.section_label);
            tv.setText("Total Notif Count: " + Integer.valueOf(NotificationObserver.notification_count) +
                    "\nNotif Clear Count: " + Integer.valueOf(NotificationObserver.notification_clear) +
                    "\nNotif Shade Open Count: " + Integer.valueOf(NotifAccessService.notification_shade_count) );

            Thread thread = new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        Thread.sleep(1000);
                    }catch(InterruptedException e){
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            tv.setText("Total Notif Count: " + Integer.valueOf(NotificationObserver.notification_count) +
                                    "\nNotif Clear Count: " + Integer.valueOf(NotificationObserver.notification_clear) +
                                    "\nNotif Shade Open Count: " + Integer.valueOf(NotifAccessService.notification_shade_count));

                        }

                    });
                }
            };


            thread.start();

            return rootView;
        }
    }

    public static class InfoFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static InfoFragment newInstance(int sectionNumber) {
            InfoFragment fragment = new InfoFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public InfoFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_notif_base_main, container, false);

            final TextView tv = (TextView)rootView.findViewById(R.id.section_label);
            tv.setText("Location (Lat,Long) : " + Double.valueOf(LocationObserver.latitude)
                    + " , " + Double.valueOf(LocationObserver.longitude) + "\n" +
                    "Wifi State : " + Short.valueOf(HardwareObserver.wifistate) + "\n" +
                    "Bluetooth State : " + Short.valueOf(HardwareObserver.bluetoothstate) + "\n" +
                    "Mic Recording : " + AudioObserver.audio_pitch_value + "\n" +
                    "Sound Level : " + AudioObserver.audio_decibel + "\n" +
                    "Prox Value : " + SensorService.prox_value );

            Thread thread = new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        Thread.sleep(100);
                    }catch(InterruptedException e){
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            tv.setText("Location (Lat,Long) : " + Double.valueOf(LocationObserver.latitude)
                                    + " , " + Double.valueOf(LocationObserver.longitude) + "\n" +
                                    "Wifi State : " + Short.valueOf(HardwareObserver.wifistate) + "\n" +
                                    "Bluetooth State : " + Short.valueOf(HardwareObserver.bluetoothstate) + "\n" +
                                    "Headphone State : " + Short.valueOf(HardwareObserver.headphones) + "\n" +
                                    "Mic Recording : " + AudioObserver.audio_pitch_value + "\n" +
                                    "Sound Level : " + AudioObserver.audio_decibel + "\n" +
                                    "Light Value : " + SensorService.light_value + "\n" +
                                    "Prox Value : " + SensorService.prox_value + "\n" +
                                    "Acc Value : " + SensorService.acc_value);

                        }

                    });
                }
            };


            thread.start();

            return rootView;
        }
    }

    public static class AboutFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static AboutFragment newInstance(int sectionNumber) {
            AboutFragment fragment = new AboutFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public AboutFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_notif_base_main, container, false);
            TextView tv = (TextView) rootView.findViewById(R.id.section_label);
            tv.setText("Thank you for participating.\n\nThis is an App developed in HP Labs " +
                    "by Swadhin and Abhinav for understanding notification usage " +
                    "behavior and corresponding context.");

            return rootView;
        }
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}

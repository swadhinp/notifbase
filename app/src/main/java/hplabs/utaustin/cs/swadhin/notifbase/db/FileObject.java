package hplabs.utaustin.cs.swadhin.notifbase.db;

/**
 * Created by swadhin on 6/24/15.
 */
public class FileObject {

    /** Report id */
    public int id = -1;

    /** timestamp of the event */
    public long timestamp;

    /** hash */
    public String hash;

    /** filename */
    public String filename;

    /** status */
    public String status;


    public FileObject(long timestamp, String hash, String filename, String status) {
        this.timestamp = timestamp;
        this.hash = hash;
        this.filename = filename;
        this.status = status;
    }

    /**
     * Constructor with report id
     * @param id
     * @param timestamp
     * @param hash
     * @param filename
     * @param status
     */
    public FileObject(int id, long timestamp, String hash, String filename, String status) {
        this.id =id;
        this.timestamp = timestamp;
        this.hash = hash;
        this.filename = filename;
        this.status = status;
    }

    /**
     * Empty Constructor
     */
    public FileObject() {

    }

    public boolean equals(FileObject r){
        return this.id == r.id;
    }

}

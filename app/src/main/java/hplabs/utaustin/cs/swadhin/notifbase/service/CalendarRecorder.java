package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.content.Context;

import java.util.TimerTask;

/**
 * Created by swadhin on 7/7/15.
 */
public class CalendarRecorder extends TimerTask{

    private Context context = null;

    public void run() {
        //Log.e("YYY", "Timer task Calendar executed");
        CalenderObserver.readCalendar(context, 0, 10);
        //Log.d("MAIN", CalenderObserver.current_event_list);
    }

    public CalendarRecorder( Context con ){
        context = con;
    }

}

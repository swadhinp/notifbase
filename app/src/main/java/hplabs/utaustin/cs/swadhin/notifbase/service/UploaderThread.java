package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;


import java.io.File;
import java.security.MessageDigest;
import java.util.TimeZone;

import hplabs.utaustin.cs.swadhin.notifbase.db.FileObject;
import hplabs.utaustin.cs.swadhin.notifbase.db.FilesDAO;
import hplabs.utaustin.cs.swadhin.notifbase.db.GeneralDAO;
import hplabs.utaustin.cs.swadhin.notifbase.R;

import hplabs.utaustin.cs.swadhin.notifbase.db.Utils;

/**
 * Background thread that uploads data to the remote server using WiFi.
 *
 * @author Abhinav Parate
 *
 */
class UploaderThread extends Thread{

    private UtilityService service = null;
    private boolean running = false;
    private String userId = "";
    private String imei = "";
    private String streamName = "";
    private static final long UPLOAD_INTERVAL = 30*60*1000;//30 minutes
    private static final long MAX_WAIT_FOR_FILE = 24*60*60*1000;//24 minutes

    UploaderThread(UtilityService service){
        this.service = service;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(service);
        Resources rcs = service.getResources();
        userId = prefs.getString(rcs.getString(R.string.pref_subject_id), "A");
        TelephonyManager manager = (TelephonyManager)(service.getSystemService(Context.TELEPHONY_SERVICE));
        imei = manager.getDeviceId();
        String tokens[] = userId.split("[ \t]+");
        if(tokens!=null)
        {
            for(int i=0;i<tokens.length;i++)
            {
                if(tokens[i]!=null && !tokens[i].trim().equals(""))
                    streamName += tokens[i].charAt(0);
            }
        }

        TimeZone tz = TimeZone.getDefault();

        int diffInMillisecs = tz.getRawOffset();
        int HOUR_IN_MILLISECS = 60*60*1000;
        int diffInHour = diffInMillisecs/HOUR_IN_MILLISECS;
        int diffInHalfHour = (diffInMillisecs%HOUR_IN_MILLISECS)/(60*1000);

        String timeZoneString = String.format("%d-%02d", diffInHour,
                diffInHalfHour);

        streamName += ("U-"+imei+"-T"+timeZoneString); // File Name to be written

    }

    @Override
    public void run(){
        running = true;
        while(true) {
            if(!running)
                break;
            Utils.d(Utils.TAG, "Uploader Thread running...");
            if(isWifiConnected(service)){
                //Read and upload files only if connected by wifi

                FileObject[] files = null;
                FilesDAO dao = null;
                //Upload files from the Files database
                try{
                    synchronized (GeneralDAO.semaphore) {

                        dao = new FilesDAO(service);
                        dao.openRead();
                        files = dao.getAllFileObjects();
                        dao.close();

                    }
                } catch(Exception e) {
                    Utils.d("Error in Files DB Read transaction at Uploader Thread: "+e.getLocalizedMessage());
                }

                long time = System.currentTimeMillis();
                //Now, upload sensor files
                for(int i=0;files!=null && i<files.length;i++){
                    if(files[i].status.equals("close")
                            || (time-files[i].timestamp)>MAX_WAIT_FOR_FILE){ //24 hour old files will be uploaded
                        if(!isWifiConnected(service))
                            break;

                        File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"notifbase");
                        File uploadFile = new File(root,files[i].filename);

                        String stream = getStream(files[i].filename);
                        String schema = getSchema(stream);
                        boolean success = uploadFile(uploadFile, schema, stream);
                        if(success) {
                            success = uploadFile.delete();
                            if(success) {
                                try{
                                    synchronized (GeneralDAO.semaphore) {

                                        //Remove entry from the database
                                        dao = new FilesDAO(service);
                                        dao.openWrite();
                                        dao.delete(files[i]);
                                        dao.close();

                                    }
                                } catch(Exception e) {
                                    Utils.d("Error in Files DB Delete transaction at Uploader Thread: "+e.getLocalizedMessage());
                                }

                            }
                        }
                    }
                }//All files in db read
            }
            //Now wait for UPLOAD_INTERVAL before trying next upload
            synchronized(this){
                try {
                    this.wait(UPLOAD_INTERVAL);
                } catch (InterruptedException e) {
                    Utils.d("Error waiting during uploader thread execution");
                }
            }

        }
    }

    private String getStream(String file){
        if(file.contains("appusage"))
            return "appusage";
        else if(file.contains("empa"))
            return "emp-accel";
        else if(file.contains("empg"))
            return "emp-gsr";
        else if(file.contains("empb"))
            return "emp-bvp";
        else if(file.contains("empi"))
            return "emp-ibi";
        else if(file.contains("empt"))
            return "emp-temp";
        else if(file.contains("activity"))
            return "activity";
        else if(file.contains("steps"))
            return "steps";
        else if(file.contains("Gyro"))
            return "gyroscope";
        else if(file.contains("Accel"))
            return "accelerometer";
        else if(file.contains("Comp"))
            return "compass";
        else if(file.contains("Q-sensor"))
            return "eda";
        else if(file.contains("Report"))
            return "reports";
        else return "quaternion";
    }

    private String getSchema(String stream){
        if(stream.contains("appusage"))
            return "aparate.appusage";
        else if(stream.contains("emp-a"))
            return "public.accel";
        else if(stream.contains("emp-g"))
            return "aparate.egsr";
        else if(stream.contains("emp-b"))
            return "aparate.ebvp";
        else if(stream.contains("emp-i"))
            return "aparate.eibi";
        else if(stream.contains("emp-t"))
            return "aparate.etemp";
        else if(stream.contains("activity"))
            return "aparate.activity";
        else if(stream.contains("steps"))
            return "aparate.steps";
        else if(stream.contains("gyro"))
            return "public.gyro";
        else if(stream.contains("accel"))
            return "public.accel";
        else if(stream.contains("compass"))
            return "public.compass";
        else if(stream.contains("eda"))
            return "public.eda";
        else if(stream.contains("report"))
            return "umassmed.report";
        else return "public.quaternion";
    }

    private boolean uploadFile(File uploadFile, String schema, String stream){


        /*********************************************************
         *********************************************************
         *
         * OVER_RIDING CASENAME AND STREAMNAME HERE
         * STREAMNAME NOW DERIVED FROM USER ID TO SAVE BURDEN ON MCROWD
         *
         *********************************************************
         *********************************************************/
        String caseName = "notifbase-app";



        Utils.d(Utils.TAG, "Received upload request:"+uploadFile);
        HttpResponse response = null;
        MultipartEntity ent = new MultipartEntity();
        try {

            //We are going to upload the files to umassmed user on mcrowdviz
            String user = "aparate";

            FileBody fb = new FileBody(uploadFile);
            ent = new MultipartEntity();
            ent.addPart("user",new StringBody(user));

            ent.addPart("casename", new StringBody(caseName));
            ent.addPart("stream", new StringBody(streamName));
            ent.addPart("schema", new StringBody(schema));

            String secret ="AX6782935njkefng235";
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest((caseName+streamName+secret).getBytes());
            StringBuffer buf = new StringBuffer(hash.length * 2);
            for (byte element: hash) {
                int intVal = element & 0xff;
                if (intVal < 0x10){
                    // append a zero before a one digit hex
                    // number to make it two digits.
                    buf.append("0");
                }
                buf.append(Integer.toHexString(intVal));
            }
            String signature =buf.toString();
            ent.addPart("signature",new StringBody(signature));
            ent.addPart("datafile",fb);

            HttpClient httpClient = Utils.createHttpClient(service);
            HttpPost request = new HttpPost("http://15.126.208.232:8383/mcrowdviz/b_uploadMcrowdFiles.jsp");
            request.setEntity(ent);
            response = httpClient.execute(request);
            if(httpClient != null && httpClient.getConnectionManager() != null) httpClient.getConnectionManager().shutdown();
            if(response!=null){
                int status = response.getStatusLine().getStatusCode();
                //Utils.d(Utils.TAG, "Received Response Status "+status +" for file "+uploadFile);
                if(status == HttpStatus.SC_OK){
                    return true;
                }
            }
        } catch (Exception e) {
            Utils.d(Utils.TAG, "Exception in http request at uploader thread:" + e.getLocalizedMessage());

        }
        return false;
    }

    public static boolean isWifiConnected(Context context){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi.isConnected();
    }

    public synchronized void setRunning(boolean running){
        this.running = running;
        notifyAll();
    }


}


package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.util.Log;

import be.tarsos.dsp.AudioDispatcher;

/**
 * Created by swadhin on 7/7/15.
 */
public class ActionThread extends Thread {

    private volatile boolean running = true;
    private AudioDispatcher adpo = null;
    private String adstr = "";

    public ActionThread(){

    }

    public ActionThread(AudioDispatcher adp, String str){

        super(adp, str);
        this.adpo = adp;
        this.adstr = str;
    }

    public void terminate() {
        running = false;
    }

    @Override
    public void run() {
        while (running) {
            try {
                //adpo.run();
                Thread.sleep((long) 10);
                //Log.e("Thread", "Processing");

            } catch (InterruptedException e) {
                Log.e("Exception", e.toString());
                adpo.stop();
                running = false;
            }
        }

    }

}

package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.KeyEvent;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import hplabs.utaustin.cs.swadhin.notifbase.NotifBaseMainActivity;
import hplabs.utaustin.cs.swadhin.notifbase.R;
import hplabs.utaustin.cs.swadhin.notifbase.db.Utils;
import hplabs.utaustin.cs.swadhin.notifbase.service.UtilityService.UtilityBinder;

/*
*<p>
        *This service is the main component for handling all background routines,for example,
        *listening to intents and starting threads.This component registers itself to
        *all the broadcasts that are of interest for appsensor;for every received
        *intent it updates the required observers.
        *</p>
        *
        *

@author Matthias Boehmer, matthias.boehmer@dfki.de
*/

public class AppSensorService extends Service {

    /** Thread for logging application usage */
    private AppObserver appLogger;

    /** Thread for logging locations */
    private LocationObserver locationLogger;

    /** Contexts needed to access additional system services */
    protected static Context context;

    private static boolean isRegistered = false;

    public static int DAY_RESET = 0;
    private static boolean ENABLED_LOGGING = true;
    public static String example_string = "";

    public static HashMap<String,String> categoryMap = new HashMap<String,String>();

    public static String launcherPackage = null;
    /** Returns null binder
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /** Receiver that reacts on screen on and off */
    private static BroadcastReceiver intentListener;

    /** Receiver that reacts to various broadcasts */
    private static BroadcastReceiver broadcastListener;

    /** Filter for intents related to screen */
    private static IntentFilter screenIntentFilter = new IntentFilter();

    /** Filter for intents related to package manager */
    private static IntentFilter packageIntentFilter = new IntentFilter();

    /** Filter for intents that are broadcasts */
    private static IntentFilter broadcastIntentFilter = new IntentFilter();

    /** intent broadcasts */
    static {

        // register for install/update/removal
        packageIntentFilter.addAction(Intent.ACTION_PACKAGE_ADDED); // added
        packageIntentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED); // uninstall
        packageIntentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED); // Update
        packageIntentFilter.addDataScheme("package");

        // register the receiver for screen on/off events
        screenIntentFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenIntentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        screenIntentFilter.addAction(Intent.ACTION_SHUTDOWN);
        screenIntentFilter.addAction(Intent.ACTION_BOOT_COMPLETED);
        screenIntentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        screenIntentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        screenIntentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        screenIntentFilter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);

        broadcastIntentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_BATTERY_LOW);
        broadcastIntentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
        broadcastIntentFilter.addAction(Intent.ACTION_BOOT_COMPLETED);
        broadcastIntentFilter.addAction(Intent.ACTION_CAMERA_BUTTON);
        broadcastIntentFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        broadcastIntentFilter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_DEVICE_STORAGE_LOW);
        broadcastIntentFilter.addAction(Intent.ACTION_DEVICE_STORAGE_OK);
        broadcastIntentFilter.addAction(Intent.ACTION_DOCK_EVENT);
        broadcastIntentFilter.addAction(Intent.ACTION_DREAMING_STARTED);
        broadcastIntentFilter.addAction(Intent.ACTION_DREAMING_STOPPED);
        broadcastIntentFilter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
        broadcastIntentFilter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
        broadcastIntentFilter.addAction(Intent.ACTION_GTALK_SERVICE_CONNECTED);
        broadcastIntentFilter.addAction(Intent.ACTION_GTALK_SERVICE_DISCONNECTED);
        broadcastIntentFilter.addAction(Intent.ACTION_INPUT_METHOD_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_LOCALE_CHANGED);
		/*broadcastIntentFilter.addAction(Intent.ACTION_MANAGED_PROFILE_ADDED);
		broadcastIntentFilter.addAction(Intent.ACTION_MANAGED_PROFILE_REMOVED);*/
        broadcastIntentFilter.addAction(Intent.ACTION_MANAGE_PACKAGE_STORAGE);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_BAD_REMOVAL);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_BUTTON);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_CHECKING);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_NOFS);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_REMOVED);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_SHARED);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTABLE);
        broadcastIntentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        broadcastIntentFilter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
		/*Package broadcasts require data scheme
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_DATA_CLEARED);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_FIRST_LAUNCH);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_FULLY_REMOVED);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_NEEDS_VERIFICATION);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_VERIFIED);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_RESTARTED);
		broadcastIntentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);*/
        broadcastIntentFilter.addAction(Intent.ACTION_PROVIDER_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_REBOOT);
        broadcastIntentFilter.addAction(Intent.ACTION_SHUTDOWN);
        broadcastIntentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        broadcastIntentFilter.addAction(Intent.ACTION_UID_REMOVED);
        broadcastIntentFilter.addAction(Intent.ACTION_USER_PRESENT);
    }

    @SuppressWarnings("unused")
    private static BroadcastReceiver batteryReceiver = new BroadcastReceiver() {
        int scale = -1;
        int level = -1;
        int voltage = -1;
        int temp = -1;
        @Override
        public void onReceive(Context context, Intent intent) {
            level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
            voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);

            if (0 == intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)) {
                // device is on battery
                HardwareObserver.powerstate = HardwareObserver.POWER_UNCONNECTED;
            } else {
                HardwareObserver.powerstate = HardwareObserver.POWER_CONNECTED;
            }

            HardwareObserver.powerlevel = (short) (100*level/scale);
        }
    };

    /** Starts or Stops various logging threads
     * @see android.app.Service#onCreate()
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mBound = false;

        context = getApplicationContext();

        Utils.d( "AppSensor Service was created -- next we bind to utility service");

        // Bind to Utility Service
        Intent intent = new Intent(this, UtilityService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

		/*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		Resources rcs = getResources();
		Editor editor = prefs.edit();
		editor.putBoolean(rcs.getString(R.string.pref_appcategories_fetched), false);
		editor.apply();
		synchronized (GeneralDAO.semaphore) {
			AppCategoryDAO dao = new AppCategoryDAO(context);
			dao.openWrite();
			dao.deleteAll();
			dao.close();
		}*/

        intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, 0);
        if(resolveInfo!=null)
            launcherPackage = resolveInfo.activityInfo.packageName;


    };

    @Override
    public int onStartCommand(Intent intent,int flags, int startId){
        super.onStartCommand(intent, flags, startId);

        // start the service for logging app usage
        appLogger = AppObserver.getAppUsageLogger(getBaseContext());

        // start the service for logging locations
        locationLogger = LocationObserver.getLocationLogger(getAppContext());


        // listen to broadcast intents
        if(intentListener == null){
            intentListener = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    String packageName = "null";
                    if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
                        packageName = intent.getDataString().replaceAll("package:", "");
                        appLogger.logAppInstalled(packageName);
                    }
                    else if (action.equals(Intent.ACTION_PACKAGE_REMOVED)) {

                        packageName = intent.getDataString().replaceAll("package:", "");
                        appLogger.logAppRemoved(packageName);
                    }
                    else if (action.equals(Intent.ACTION_PACKAGE_CHANGED) || action.equals(Intent.ACTION_PACKAGE_REPLACED)) {

                        packageName = intent.getDataString().replaceAll("package:", "");
                        appLogger.logAppUpdated(packageName);
                    }
                    else if (action.equals(Intent.ACTION_SCREEN_ON)) {

                        AppUsageEvent aue = new AppUsageEvent();
                        aue.taskID = -1;
                        aue.packageName = "NotifAction";
                        aue.className = "NotifAction";
                        //aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_ON;

                        aue.eventtype = "event_notification_act";
                        aue.eventtype_info = AppUsageEvent.EVENT_UNDEFINED_INFO;

                        //aue.starttime = notification.getPostTime(); //Time when it was posted
                        aue.starttime = System.currentTimeMillis(); //Time when it was posted
                        aue.runtime = 0; // since we don't know when it ends
                        aue.longitude = LocationObserver.longitude;
                        aue.latitude = LocationObserver.latitude;
                        aue.accuracy = LocationObserver.accuracy;
                        aue.powerstate = HardwareObserver.powerstate;
                        aue.wifistate = HardwareObserver.wifistate;
                        aue.bluetoothstate = HardwareObserver.bluetoothstate;
                        aue.headphones = HardwareObserver.headphones;
                        aue.orientation = HardwareObserver.orientation;
                        aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;
                        String temp_str = "spl-pitch-rms-mfcc:";
                        aue.service_info = NotifBaseMainActivity.service_info;

                        try {
                            aue.audio_info = new String(Compressor.compress(temp_str.getBytes( "ISO-8859-1" )), "ISO-8859-1");

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        aue.notif_info = NotificationObserver.active_notif_arr;
                        aue.calendar_event_str = CalenderObserver.current_event_list;
                        //aue.access_info = "None";
                        AppSensorService.logEvent(aue);

                        HardwareObserver.screenState = HardwareObserver.SCREEN_ON;
                        HardwareObserver.timestampOfLastScreenOn = System.currentTimeMillis();
                        appLogger.logDeviceScreenOn();
                        appLogger.startLogging();
                    }
                    else if (action.equals(Intent.ACTION_SCREEN_OFF)) {

                        HardwareObserver.screenState = HardwareObserver.SCREEN_OFF;
                        appLogger.logDeviceScreenOff();
                        appLogger.checkStandByOnSceenOff();
                    }
                    else if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                        String wifiState = HardwareObserver.wifiChanged(intent);
                        appLogger.logWifiStateChanged(wifiState);
                    }
                    else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                        HardwareObserver.networkChanged(intent);
                    }
                    else if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                        String headphoneState = HardwareObserver.headphonesChanges(intent);
                        appLogger.logHeadphonesStateChanged(headphoneState);
                    }
                    else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                        String btState = HardwareObserver.bluetoothChanges(intent);
                        appLogger.logBluetoothStateChanged(btState);
                    }
                    else if (action.equals(AudioManager.RINGER_MODE_CHANGED_ACTION)) {
                        String ringerMode = HardwareObserver.ringerModeChanges(intent);
                        appLogger.logRingerStateChanged(ringerMode);
                    }
                    else {
                        Utils.d( "unhandled: " + intent.getAction());
                    }
                }
            };
        }

        //listen to general broadcasts
        if(broadcastListener == null){
            broadcastListener = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    String toks[] = action.split("\\.");
                    String suffix = toks[toks.length-1];
                    if(suffix.startsWith("AIRPLANE_")) {
                        boolean state = intent.getBooleanExtra("state", false);
                        appLogger.logBroadcastEvent(-1, "airplane_mode_"+(state?"on":"off"));
                    } else if(suffix.startsWith("CAMERA")) {
                        KeyEvent event = (KeyEvent)intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
                        //long time = event.getEventTime();
                        long time = System.currentTimeMillis();
                        appLogger.logBroadcastEvent(time, "bc_"+suffix.toLowerCase(Locale.ENGLISH));
                    } else if (suffix.startsWith("TIMEZONE")) {
                        String tz = TimeZone.getDefault().getDisplayName();//.getCurrentTimezone();
                        appLogger.logBroadcastEvent(-1, "Timezone_"+tz.replaceAll(",","|"));
                    } else {
                        appLogger.logBroadcastEvent(-1, "bc-"+suffix.toLowerCase(Locale.ENGLISH));
                    }
                    //Utils.d("SARAYU","BC "+" "+suffix.toLowerCase(Locale.ENGLISH));
                }
            };
        }



        // read current bluetooth state
        if (BluetoothAdapter.getDefaultAdapter() != null) {
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                HardwareObserver.bluetoothstate = HardwareObserver.BLUETOOTH_ON;
            } else {
                HardwareObserver.bluetoothstate = HardwareObserver.BLUETOOTH_OFF;
            }
        } else {
            HardwareObserver.bluetoothstate = HardwareObserver.BLUETOOTH_OFF;
        }

        //checkLoggingStatus();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Resources rcs = getResources();


        boolean activate = prefs.getBoolean(rcs.getString(R.string.pref_appsensor_active), true);
        startLogging(activate);

        //new FetchAppCategoriesTask().execute((Void)null);

        return START_STICKY;
    }

    /** Destroy Service
     * @see android.app.Service#onDestroy()
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this,NotificationObserver.class));
        startLogging(false);
        Utils.d( "service was destroyed");
    }

    /**
     * The service can be started by using this method. Calling this method can
     * only be beneficial, maybe the user has stopped the service. However, the
     * service will only start if the disclaimer was acknowledged.
     *
     * @param c
     */
    public static void startByIntent(Context c) {
        Utils.d( "Starting AppSensorService by Intent");
        Intent starter = new Intent(c, AppSensorService.class);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startService(starter);
    }

    /**
     * Get WifiManager
     * @return instance of wifi manager
     */
    public static WifiManager getWifiManager(){
        if(context!=null)
            return   (WifiManager)context.getSystemService(WIFI_SERVICE);
        return null;
    }

    /**
     * Get wifi info
     * @return instance of wifi info
     */
    public static WifiInfo getWifiInfo(){
        WifiManager wifiManager = getWifiManager();
        if(wifiManager!=null)
            return wifiManager.getConnectionInfo();
        return null;
    }

    private static boolean getWifiConnected()
    {
        if(context==null)
            return false;
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifi.isAvailable()  && wifi.isConnected();
    }

    public static Context getAppContext(){
        return context;
    }


    /**
     * Activates/Deactivates Logging
     * @param activate
     */
    private void startLogging(boolean activate) {
        if (activate) {
            AppObserver.getAppUsageLogger(this).startLogging();
            locationLogger.startLogging();
            isRegistered = true;
            // register with intent filter
            registerReceiver(intentListener, screenIntentFilter);
            registerReceiver(intentListener, packageIntentFilter);
            registerReceiver(intentListener, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
            registerReceiver(intentListener, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
            registerReceiver(batteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            registerReceiver(broadcastListener, broadcastIntentFilter);
            startService(new Intent(this,NotificationObserver.class));
            if(!mBound){
                Intent intent = new Intent(this, UtilityService.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }
        } else {
            AppObserver.getAppUsageLogger(this).pauseLogging();
            locationLogger.pauseLogging();
            if(isRegistered){
                unregisterReceiver(intentListener);
                unregisterReceiver(batteryReceiver);
                unregisterReceiver(broadcastListener);
                isRegistered = false;
            }
            //Stop writing log
            if(logWriter!=null && mBound)
                mUtilService.closeLogWriter(logWriter);
            logWriter = null;
            logsWritten=0;
            stopService(new Intent(this,NotificationObserver.class));
			/*if(mBound){
				unbindService(mConnection);
				mBound = false;
			}*/

        }
    }

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //---------Use of utility service to log data-----------------------------------
    //------------------------------------------------------------------------------

    private static boolean mBound = false;
    private static UtilityService mUtilService = null;
    private static LogWriter logWriter = null;
    private static int logsWritten =0;
    private static int MAX_LOGS = 200;

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            UtilityBinder binder = (UtilityBinder) service;
            mUtilService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }

    };

    protected static void logEvent(AppUsageEvent aue){

        if(mBound){
            if(ENABLED_LOGGING)
            {
                if(logWriter==null || logWriter.isClosed())
                {
                    logWriter = mUtilService.getLogWriter("appusage");
                    logsWritten = 0;
                }
                if(logWriter!=null)
                {
                    logWriter.writeLog(aue.toString());
                    example_string = aue.toString();

                    logsWritten++;
                    if(logsWritten>MAX_LOGS){
                        mUtilService.closeLogWriter(logWriter);
                        logWriter = null;
                    }
                }
            }
        }
    }


}


package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import java.util.List;
import java.util.TimerTask;

import hplabs.utaustin.cs.swadhin.notifbase.NotifBaseMainActivity;
import hplabs.utaustin.cs.swadhin.notifbase.R;

/**
 * Created by swadhin on 7/24/15.
 */
public class ServicePermissionChecker extends TimerTask {

    private Context context = null;


    public ServicePermissionChecker( Context con ){
        context = con;
    }

    public void run() {
        //Log.e("YYY", "Timer task executed");
        boolean acc_flag_n = isAccessibilityEnabled(context, "hplabs.utaustin.cs.swadhin.notifbase/.service.NotifAccessService");

        if(acc_flag_n == true){
            NotifBaseMainActivity.service_info = "1";
        }else{
            createNotificationText();
        }

    }


    private boolean isAccessibilityEnabled(Context context, String id) {

        AccessibilityManager am = (AccessibilityManager) context
                .getSystemService(Context.ACCESSIBILITY_SERVICE);

        List<AccessibilityServiceInfo> runningServices = am
                .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
        for (AccessibilityServiceInfo service : runningServices) {
            if (id.equals(service.getId())) {
                return true;
            }
        }

        return false;
    }

    public void createNotificationText() {
        // Prepare intent which is triggered if the
        // notification is selected

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // Build notification
        // Actions are just fake
        Notification noti = new NotificationCompat.Builder(context)
                .setContentTitle("Notifbase")
                .setContentText("Accessibility Permission Needed").setSmallIcon(R.drawable.ic_launcher)
                .setSound(uri)
                .setTicker("Notifbase")
                .setContentIntent(pIntent).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(1, noti);

    }


}
package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.app.Service;
import android.app.admin.DeviceAdminReceiver;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import hplabs.utaustin.cs.swadhin.notifbase.R;
import hplabs.utaustin.cs.swadhin.notifbase.db.FileObject;
import hplabs.utaustin.cs.swadhin.notifbase.db.FilesDAO;
import hplabs.utaustin.cs.swadhin.notifbase.db.Utils;

/**
 * This service manages operation of various sensors and provides logging utility to
 * the sensors in execution.
 *
 * @author Abhinav Parate
 *
 */
public class UtilityService extends Service {







    private static UploaderThread uploader = null;

    /**
     * Class used for the client Binder. For interaction with service
     * without IPC
     */
    public class UtilityBinder extends Binder {
        public UtilityService getService() {
            // Return this instance of LocalService so clients can call public methods
            return UtilityService.this;
        }
    }

    // Binder given to clients
    private final IBinder mBinder = new UtilityBinder();

    /**
     * Background receiver to start service when the phone boots
     * @author Abhinav Parate
     *
     */
    public static class UtilityService_BGReceiver extends DeviceAdminReceiver {
        public void onReceive(Context context, Intent intent){
            Intent bootUp = new Intent(context, UtilityService.class);
            context.startService(bootUp);
        }
    }


    /**
     * Background receiver to start data uploader when wifi connection is available.
     * @author Abhinav Parate
     *
     */
    public static class UtilityService_WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            final android.net.NetworkInfo wifi = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (wifi.isAvailable()  && wifi.isConnected()) {

                if(uploader!=null){
                    Utils.d("WiFi Network Available. Starting Uploader ");
                    uploader.setRunning(true);

                    AppSensorService.startByIntent(context);
                }
            }
        }
    };


    /* (non-Javadoc)
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    //-------------------------------------------------------------------------------
    //PREFERENCE MANAGEMENT
    //-------------------------------------------------------------------------------
    private boolean ENABLED_APPSENSOR = false;


    @Override
    public void onCreate() {
        super.onCreate();
        Utils.d("utility service created");
        //Start uploader thread
        uploader = new UploaderThread(this);
        uploader.start();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uploader.setRunning(false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        readPreferences();
        startServices();
        return START_STICKY; // run until explicitly stopped.
    }


    //TODO: Decide if we want to have a persistent notification.
    /**
     * Shows notification to make sure that the service is a foreground service
     */
    @SuppressWarnings("unused")
    /*
    private void showNotification() {
        //if(notifiedStatus)
        //return;
        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, NotifBaseMainActivity.class), 0);

        // Use the commented block of code if your target environment is Android-16 or higher
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle("Sarayu")
                .setContentText("Service is running").setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(contentIntent);
        Notification notification = builder.build();

        notification.flags|=Notification.FLAG_NO_CLEAR;

        //startForeground(NOTIFICATION_ID, notification);
        //notifiedStatus = true;
    }*/

    /**
     * Cancel notification when the sensor data is not being logged
     */
    //@SuppressWarnings("unused")
    private void cancelNotification() {
        stopForeground(true);
    }

    /**
     * Reads saved preferences
     */
    private void readPreferences() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Resources rcs = getResources();
        ENABLED_APPSENSOR = prefs.getBoolean(rcs.getString(R.string.pref_appsensor_active), false);
    }

    private void startServices(){
        if(ENABLED_APPSENSOR) AppSensorService.startByIntent(getBaseContext());
    }


    //-----------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------
    //NOTE: Logging Utilities
    //-----------------------------------------------------------------------------------------------


    /**
     * Returns a log writer for a stream
     * @param streamTag sensor stream identifier
     * @return null if failed to create a log writer instance
     */
    public LogWriter getLogWriter(String streamTag){
        File rootDir = getStorageLocation();

        long time = System.currentTimeMillis();
        String fileName = streamTag+"_"+time+".csv";
        Utils.d("Asked for logwriter "+fileName);
        BufferedWriter out = null;
        try{
            out = new BufferedWriter(new FileWriter(new File(rootDir,fileName)));
            LogWriter lw = new LogWriter(out);
            addFileToDB(fileName, lw.toString(), "open", time);
            return lw;
        }catch(IOException e){
            e.printStackTrace();
        }
        out = null;
        return null;
    }

    /**
     * Close the log writer for a stream
     * @param out log writer instance
     */
    public void closeLogWriter(LogWriter out) {
        if(out!=null){
            out.close();
            FilesDAO dao = new FilesDAO(this);
            try{
                dao.openWrite();
                FileObject fo = FilesDAO.cursor2file(dao.findByHash(out.toString()));
                fo.status = "close";
                dao.update(fo);
                dao.close();
                Utils.d(Utils.TAG,"File "+fo.filename+" added to upload queue");
            }catch(Exception e){
                e.printStackTrace();
            }

            //Run uploader now that file is ready
            if(uploader!=null)
                uploader.setRunning(true);
        }

    }

    private void addFileToDB(String fileName, String hash, String status, long time){
        FileObject fo = new FileObject(time, hash, fileName, status);
        FilesDAO dao = new FilesDAO(this);
        dao.openWrite();
        dao.insert(fo);
        dao.close();
    }


    /**
     * Returns a root directory where the logging takes place
     * @return file instance of storage location
     */
    private static File getStorageLocation(){
        File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"notifbase");
        if(!root.exists())
            root.mkdir();
        return root;
    }


}


package hplabs.utaustin.cs.swadhin.notifbase;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by swadhin on 7/6/15.
 */


public class PermissionAlertDialogFragment extends DialogFragment {

    private static Activity act_cur = null;
    private static String strTitle = "";
    private static String[] strArray = null;

    public static PermissionAlertDialogFragment newInstance(int title, Activity act, String str_title, String[] str_array) {
        PermissionAlertDialogFragment frag = new PermissionAlertDialogFragment();

        act_cur = act;
        strTitle = str_title;
        strArray = str_array.clone();

        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int title = getArguments().getInt("title");

        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(act_cur);

        alertDialogBuilder.setTitle(strTitle);

        alertDialogBuilder.setItems(strArray,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //rest of your implementation

                        if( which == 0) {

                            Toast.makeText(act_cur, Integer.toString(which), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                            startActivity(intent);

                        }
                        else if( which == 1){

                            Toast.makeText(act_cur, Integer.toString(which), Toast.LENGTH_SHORT).show();

                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {

                                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                                startActivity(intent);

                            } else {

                                Toast.makeText(act_cur, "Thank you for granting the permissions", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{

                            Toast.makeText(act_cur, Integer.toString(which), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                            startActivity(intent);
                        }
                    }
                });

        return alertDialogBuilder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog){

        Toast.makeText(act_cur, "OnCancel", Toast.LENGTH_SHORT).show();

        List<String> d_arr = checkAndPopulateDialog();

        if(d_arr.size() == 0){

            this.setCancelable(true);
            this.dismiss();

        }
        super.onCancel(dialog);

    }

    @Override
    public void onDismiss(DialogInterface dialog){

        List<String> d_arr = checkAndPopulateDialog();
        Toast.makeText(act_cur, "OnDismiss" + Integer.toString(d_arr.size()), Toast.LENGTH_SHORT).show();

        if(d_arr.size() == 0){

            this.setCancelable(true);
            this.dismiss();

        }

        super.onDismiss(dialog);

    }

    @Override
    public void onStop(){


        List<String> d_arr = checkAndPopulateDialog();

        Toast.makeText(act_cur, "OnStop" + Integer.toString(d_arr.size()), Toast.LENGTH_SHORT).show();
        if(d_arr.size() == 0){

            this.setCancelable(true);
            this.dismiss();

        }

        super.onStop();
    }

    private List<String> checkAndPopulateDialog() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(act_cur);
        boolean not_flag = prefs.getBoolean("pref_notificationaccess", false);
        boolean app_flag = prefs.getBoolean("pref_appusage", false);
        boolean acc_flag = prefs.getBoolean("pref_accessibilityaccess", false);

        List<String> dial_arr = new ArrayList<>();

        if(not_flag == false){

            dial_arr.add("Please Give Notification Access");
        }

        if(app_flag == false){

            dial_arr.add("Please Give Application Access");
        }

        if(acc_flag == false){

            dial_arr.add("Please Give Accessibility Access");
        }

        return dial_arr;
    }
}
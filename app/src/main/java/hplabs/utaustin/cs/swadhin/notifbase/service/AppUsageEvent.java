package hplabs.utaustin.cs.swadhin.notifbase.service;

import java.io.IOException;

import hplabs.utaustin.cs.swadhin.notifbase.NotifBaseMainActivity;

/**
 * <p>
 * This is a value object for the information required to observe app
 * interaction such as usage and installations. Additionally it also keeps track
 * of the context, that this event took place in (mainly local time, location).
 * </p>
 *
 * <p>Apart from app interactions, this object can record various events:</p>
 * <ul>
 * <li>Notification arrived</li>
 * <li>Notification cleared</li>
 * <li>Application used</li>
 * <li>Phone booted</li>
 * <li>Screen on/off</li>
 * <li>WiFi off/on/connected</li>
 * <li>Bluetooth off/on/connected</li>
 * <li>Headphones connected/disconnected</li>
 * </ul>
 *
 * @author Matthias Boehmer, matthias.boehmer@dfki.de
 */
class AppUsageEvent {

    // ---------------------------------------------
    // constants
    // ---------------------------------------------

    public static final String EVENT_UNDEFINED = "event_undefined";
    public static final String EVENT_UNDEFINED_INFO = "event_undefined_info";
    public static final String EVENT_APPUSE = "event_appuse";
    public static final String EVENT_INSTALLED = "event_installed";
    public static final String EVENT_UNINSTALLED = "event_uninstalled";
    public static final String EVENT_UPDATE = "event_update";

    public static final String EVENT_NOTIFICATION_ON = "event_notification";
    public static final String EVENT_NOTIFICATION_OFF = "event_notification_clear";

    public static final String EVENT_BOOT = "event_boot";
    public static final String EVENT_POWEROFF = "event_poweroff";

    public static final String EVENT_SCREENON = "event_screenon";
    public static final String EVENT_SCREENOFF = "event_screenoff";

    public static final String EVENT_WIFIOFF = "event_wifioff";
    public static final String EVENT_WIFION = "event_wifion";
    public static final String EVENT_WIFICONNECTED = "event_wificonnected";

    public static final String EVENT_BTOFF = "event_btoff";
    public static final String EVENT_BTON = "event_bton";
    public static final String EVENT_BTCONNECTED = "event_btconnected";

    public static final String EVENT_HEADPHONESON = "event_headphoneson";
    public static final String EVENT_HEADPHONESOFF = "event_headphonesoff";

    public static final String EVENT_RINGNORMAL = "event_ringnormal";
    public static final String EVENT_RINGSILENT = "event_ringsilent";
    public static final String EVENT_RINGVIBRATE = "event_ringvibrate";

    public static final String EVENT_LOCATION = "event_location";
    public static final String EVENT_ACCESS = "event_access";
    public static final String EVENT_SENSOR = "event_sensor";




    // ---------------------------------------------
    // public properties
    // ---------------------------------------------

    /** the app */
    public String packageName;
    public String className;

    // information on the interaction itself
    public String eventtype = EVENT_UNDEFINED;
    public String eventtype_info = EVENT_UNDEFINED_INFO;

    public long starttime = System.currentTimeMillis();

    /**
     * This is the timespan in milliseconds that the app was used.
     */
    public long runtime;

    public int taskID;

    /**
     * This is the UTC timestamp of the last screen on event, i.e. the start of
     * the session where the current app interaction is embedded in. It might be
     * zero, e.g. if the background logger starts within a session (e.g. when
     * AppSensor is just being installed).
     */
    public long timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;

    /* location information */
    public double longitude = LocationObserver.longitude;
    public double latitude = LocationObserver.latitude;
    public double accuracy = LocationObserver.accuracy;

    /** battery level */
    public short powerstate = HardwareObserver.powerstate;
    public short powerlevel = HardwareObserver.powerlevel;

    /** whether wifi is on, off or connected */
    public short wifistate = HardwareObserver.wifistate;

    /** whether bluetooth is on, off or connected */
    public short bluetoothstate = HardwareObserver.bluetoothstate;

    /** the most often measured orientation of the device during this event (portrait or landscape) */
    public short orientation = HardwareObserver.orientation;

    /** state of the headphones of the device, i.e. connected or not */
    public short headphones = HardwareObserver.headphones;

    public String calendar_event_str =  CalenderObserver.current_event_list;
    public String audio_info = "spl-pitch-rms-mfcc:";
    public String notif_info = "!!$!!None$@$!!$!!|";
    public String access_info = "|!!$!!None";
    public String sensor_info = "None";
    public String service_info = NotifBaseMainActivity.service_info;



    // ---------------------------------------------
    // constructors
    // ---------------------------------------------

    public AppUsageEvent(){

        try {
            audio_info = new String(Compressor.compress("spl-pitch-rms-mfcc:".getBytes("ISO-8859-1")),"ISO-8859-1");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            sensor_info = new String(Compressor.compress("None".getBytes("ISO-8859-1")),"ISO-8859-1");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public AppUsageEvent(int taskID, String packagename, long starttime, int runtime, String eventtype){
        this.taskID = taskID;
        this.packageName = packagename;
        this.starttime = starttime;
        this.runtime = runtime;
        this.eventtype = eventtype;
    }

    public AppUsageEvent(String packagename, long starttime, int runtime, String eventtype) {
        this.taskID = 0;
        this.packageName = packagename;
        this.starttime = starttime;
        this.runtime = runtime;
        this.eventtype = eventtype;
    }

    // ---------------------------------------------
    // technical methods
    // ---------------------------------------------

    @Override
    public String toString() {

        /*Log.e("APPUSAGE", starttime + "," + eventtype + "," + eventtype_info + "," + packageName + "," + className + "," + runtime + "," + bluetoothstate + ","
                + wifistate + "," + powerstate + "," + powerlevel + "," + String.valueOf(headphones) + "," + latitude + ","
                + longitude + "," + Double.valueOf(accuracy) + "," + Short.valueOf(orientation) + ","
                + Long.valueOf(timestampOfLastScreenOn) + "," + notif_info + "," + service_info + "," +
                calendar_event_str + "," + access_info + "!!$!!" + sensor_info + "!!$!!" + audio_info);*/

        return
                starttime+","+eventtype+","+eventtype_info+","+packageName+","+className +","+runtime+","+bluetoothstate+","
                        +wifistate+","+powerstate+","+powerlevel+","+String.valueOf(headphones)+","+latitude+","
                        +longitude+","+Double.valueOf(accuracy)+","+Short.valueOf(orientation)+","
                        +Long.valueOf(timestampOfLastScreenOn)+","+notif_info+","+service_info +","+
                        calendar_event_str + "," + access_info+"!!$!!" +sensor_info + "!!$!!" + audio_info;
    }


    public short getOrientationSignum() {
        if (orientation < 0 ) return -1;
        if (orientation > 0 ) return +1;
        return 0;
    }



}

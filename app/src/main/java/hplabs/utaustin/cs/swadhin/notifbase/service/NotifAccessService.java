package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import hplabs.utaustin.cs.swadhin.notifbase.NotifBaseMainActivity;

/**
 * Created by swadhin on 6/19/15.
 */
public class NotifAccessService extends AccessibilityService {

    public static final String LOG_TAG = "Notifbase_Access_Info";
    public static String audio_feature_str = "spl-pitch-rms-mfcc:";
    int mProvidedFeedbackType;

    public static boolean isRecording = false;
    private Boolean audio_flag = true;
    private volatile Boolean audio_flag_log = true;


    public static int notification_shade_count = 0;


    //@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {


        if ( (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) ||
                (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) ||
                (event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED) ||
                (event.getEventType() == AccessibilityEvent.TYPE_ANNOUNCEMENT) ) {

            //Log.i(LOG_TAG, "onAccessibilityEvent");
            Log.i(LOG_TAG, event.getText() + " : "
                    + event.getPackageName().toString() + ":"
                    + AccessibilityEvent.eventTypeToString(event.getEventType()) + ":"
                    + event.getClassName());

            //Audi/o Recording in Notification posting
            //if (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {


            //}

            CharSequence chs = event.getPackageName();

            StringBuilder sb = null;
            if( null != chs ) {
                sb = new StringBuilder(chs.length());
                sb.append(chs);
            }

            String pkg_name = "None";

            if( null != sb ) {
                pkg_name = sb.toString();
            }

            //Some Info Logging
            if ((event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) &&
                    pkg_name.equals("com.android.systemui")) {

                notification_shade_count = notification_shade_count + 1;
            }

            AppUsageEvent aue = new AppUsageEvent();

            if( (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED)){
                //Audio Data Recording

                if( (isRecording == false) && (audio_flag == true)) {

                    isRecording = true;
                    audio_flag = false;
                    audio_flag_log = false;
                    AudioObserver.mfcc_list.clear();
                    AudioObserver.spl_list.clear();
                    AudioObserver.pitch_list.clear();
                    AudioObserver.rms_list.clear();

                    final AudioObserver audObserver = new AudioObserver();

                    AudioDispatcher dispat = AudioDispatcherFactory.fromDefaultMicrophone(AudioObserver.sample_rate,
                            AudioObserver.audio_buffer_size, AudioObserver.audio_buffer_overlap);
                    //AudioDispatcher dispat = AudioDispatcherFactory.defaultMicrophoneAudioDispatcher(AudioObserver.audio_buffer_size, AudioObserver.audio_buffer_overlap);

                    if (null != dispat) {

                        audObserver.setAudioDispatcher(dispat);
                        audObserver.init();

                        //audObserver.runAudioObserver();

                        //long cur_time = System.currentTimeMillis();

                        final Thread ath = new Thread(audObserver.getAudioDispatcher(), "Audio Dispatcher");
                        ath.start();

                        new Timer(true).schedule(new TimerTask() {
                            public void run() {

                                ath.interrupt();
                                if (ath.isAlive()) {
                                    Log.d(LOG_TAG, "Alive");
                                }

                                audObserver.getAudioDispatcher().stop();
                                audObserver.setAudioDispatcher(null);

                                String tot_str = "spl-pitch-rms-mfcc:";

                                for (String s : AudioObserver.spl_list) {
                                    tot_str += s + "|";
                                }

                                tot_str += ":";

                                for (String s : AudioObserver.pitch_list) {
                                    tot_str += s + "|";
                                }

                                tot_str += ":";

                                for (String s : AudioObserver.rms_list) {
                                    tot_str += s + "|";
                                }

                                tot_str += ":";
                                for (String s : AudioObserver.mfcc_list) {
                                    tot_str += s + "|";
                                }


                                Log.i("AUDIO", tot_str);
                                audio_feature_str = tot_str;
                                isRecording = false;

                            }
                        },  10000); //10 second audio feature after notification posting
                    }
                }
            }

            new Timer(true).schedule(new TimerTask() {
                public void run() {
                    audio_flag = true;
                    audio_flag_log = true;
                }
            }, 900000); //Wait for 15 minute at least before starting SensorService again


            if( audio_flag_log == true) {
                try {
                    aue.audio_info = new String(Compressor.compress(audio_feature_str.getBytes("ISO-8859-1")), "ISO-8859-1");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            aue.taskID = event.getWindowId(); //windowid
            aue.packageName = pkg_name;

            CharSequence chs1 = event.getClassName();

            StringBuilder sb1;

            if( null != chs1 ) {
                sb1 = new StringBuilder(chs1.length());
                sb1.append(chs);
            }
            String class_name = "None";

            if( null != sb ) {
                class_name = sb.toString();
            }

            aue.className = class_name;

            aue.eventtype_info = AccessibilityEvent.eventTypeToString(event.getEventType());
            //aue.eventtype = AppUsageEvent.EVENT_NOTIFICATION_ON;

            aue.eventtype = "event_access";

            //aue.starttime = event.getEventTime(); //Time when it was posted
            aue.starttime = System.currentTimeMillis(); //Time when it was posted
            aue.runtime = 0; // since we don't know when it ends
            aue.longitude = LocationObserver.longitude;
            aue.latitude = LocationObserver.latitude;
            aue.accuracy = LocationObserver.accuracy;
            aue.powerstate = HardwareObserver.powerstate;
            aue.wifistate = HardwareObserver.wifistate;
            aue.bluetoothstate = HardwareObserver.bluetoothstate;
            aue.headphones = HardwareObserver.headphones;
            aue.orientation = HardwareObserver.orientation;
            aue.timestampOfLastScreenOn = HardwareObserver.timestampOfLastScreenOn;


            try {
                aue.audio_info = new String(Compressor.compress(audio_feature_str.getBytes("ISO-8859-1")), "ISO-8859-1");

            } catch (IOException e) {
                e.printStackTrace();
            }

            aue.calendar_event_str = CalenderObserver.current_event_list;
            aue.notif_info = "!!$!!None$@$!!$!!|";
            aue.service_info = NotifBaseMainActivity.service_info;

            String listString = getEventText(event);
            String encryptedString = "";

            List<String> list_str = new ArrayList<String>(Arrays.asList(listString.trim().split("[\\p{Punct}\\s]+"))); //Punctuation mark split

            for (String s : list_str)
            {
                //Log.d("ACC", s);
                try {
                    MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                    messageDigest.update(s.getBytes("ISO-8859-1"));
                    byte[] bt_arr = messageDigest.digest();
                    encryptedString += new String(bt_arr,"ISO-8859-1") + "$@$";

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

            String stored_string = "None";
            String current_string = listString.trim();

            if( current_string.equals("Notification shade.") || current_string.equals("Home") ||
                    current_string.equals("APPS") || current_string.equals("OK") ||
                    current_string.equals("Quick Settings.") || current_string.equals("Clear all notifications.") ||
                    current_string.equals("Notification dismissed.") || current_string.equals("Close notification")){

                stored_string = current_string;
            }

            String[] res_string = current_string.split("[\\p{Punct}\\s]+");
            stored_string = "";

            int count = 0;

            for(String str1:res_string){

                count++;
                stored_string += str1 + " ";
                if( count > 4){ //Only store the first 4 accessibility info in plain text
                    break;
                }
            }

            //Log.i(LOG_TAG,"$" + stored_string.trim() + "$");

            aue.access_info = stored_string.trim() + "|!!$!!" + encryptedString.trim();

            try {
                aue.sensor_info = new String(Compressor.compress("None".getBytes("ISO-8859-1")), "ISO-8859-1");

            } catch (IOException e) {
                e.printStackTrace();
            }



            AppSensorService.logEvent(aue);

            /*writeToFile(aue.toString());
            String str = readFromFile();
            List<String> list_str1 = new ArrayList<String>(Arrays.asList(str.trim().split(",")));
            int count = 0;

            String comp_str = "";
            for(String st: list_str1){

                if(count ==20){
                    try {
                        comp_str += Compressor.uncompress(st.getBytes("ISO-8859-1")) + "||";
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (DataFormatException e) {
                        e.printStackTrace();
                    }
                }else{
                    comp_str += st + Integer.toString(count) +"||";
                }
                count++;
            }

            Log.d("TEST", comp_str);*/

            //Log.d(LOG_TAG, Long.toString(event.getEventTime()) + ":" + getEventText(event) + " : " + event.getPackageName().toString() + ":"
            //      + AccessibilityEvent.eventTypeToString(event.getEventType()) + ":"
            //    + event.getClassName() + ":" + listString + "|" + encryptedString);
        }

    }


    private String getEventText(AccessibilityEvent event) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : event.getText()) {
            sb.append(s);
            sb.append(" ");
        }
        return sb.toString();
    }


    @Override
    protected void onServiceConnected() {
        Log.i(LOG_TAG,"onServiceConnected");

        AccessibilityServiceInfo info = new AccessibilityServiceInfo();


        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.flags =  AccessibilityServiceInfo.FLAG_REQUEST_TOUCH_EXPLORATION_MODE;
        info.notificationTimeout = 100;

        setServiceInfo(info);
        super.onServiceConnected();

        switch(info.feedbackType){

            case AccessibilityServiceInfo.FEEDBACK_AUDIBLE:
                Log.i(LOG_TAG, "notification audible");
                break;
            case AccessibilityServiceInfo.FEEDBACK_HAPTIC:
                Log.i(LOG_TAG, "notification haptic");
                break;
            case AccessibilityServiceInfo.FEEDBACK_VISUAL:
                Log.i(LOG_TAG, "notification visual");
                break;
            case AccessibilityServiceInfo.FEEDBACK_BRAILLE:
                Log.i(LOG_TAG, "notification Braille");
                break;
            case AccessibilityServiceInfo.FEEDBACK_SPOKEN:
                Log.i(LOG_TAG, "notification Spoken");
                break;
            case AccessibilityServiceInfo.FEEDBACK_GENERIC:
                Log.i(LOG_TAG, "notification Generic");
                break;
            default:
                Log.i(LOG_TAG, "notification other feedback");
                break;

        }

    }

    @Override
    public void onInterrupt() {

        Log.i(LOG_TAG , "onInterrupt");
        //Toast.makeText(getApplicationContext(), "onInterrupt", Toast.LENGTH_SHORT).show();

    }


    public void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config_new.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("config_new.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}


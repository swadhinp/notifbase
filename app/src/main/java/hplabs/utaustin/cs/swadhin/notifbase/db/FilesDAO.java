package hplabs.utaustin.cs.swadhin.notifbase.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.LinkedList;

/**
 * Data access object for the data files being managed by the app.
 *
 * <p/>This class provides access methods to the `Files' database that
 * tracks files that need to be uploaded to the server.
 *
 * @author Abhinav Parate
 */

public class FilesDAO extends GeneralDAO {

    // --------------------------------------------
    // SCHEMA
    // --------------------------------------------

    public static String TABLE_NAME = "files";

    public static final String CNAME_ID = "_id";
    public static final String CNAME_TIMESTAMP = "timestamp";
    public static final String CNAME_HASH = "hash";
    public static final String CNAME_FILENAME = "filename";
    public static final String CNAME_STATUS = "status";

    public static final String[] PROJECTION = {
            CNAME_ID,
            CNAME_TIMESTAMP,
            CNAME_HASH,
            CNAME_FILENAME,
            CNAME_STATUS
    };

    public final static int CNUM_ID = 0;
    public final static int CNUM_TIMESTAMP = 1;
    public final static int CNUM_HASH = 2;
    public final static int CNUM_FILENAME = 3;
    public final static int CNUM_STATUS = 4;


    public static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
            CNAME_ID + " INTEGER PRIMARY KEY, " +
            CNAME_TIMESTAMP + " LONG, " +
            CNAME_HASH + " TEXT, " +
            CNAME_FILENAME + " TEXT, " +
            CNAME_STATUS + " TEXT " +
            ");";

    // --------------------------------------------
    // QUERIES
    // --------------------------------------------

    private final static String WHERE_ID = CNAME_ID + "=?";
    private final static String WHERE_HASH = CNAME_HASH + "=?";
    private final static String WHERE_STATUS = CNAME_STATUS + "=?";

    // --------------------------------------------
    // LIVECYCLE
    // --------------------------------------------

    public FilesDAO(Context context) {
        super(context);
    }

    // --------------------------------------------
    // CRUD
    // --------------------------------------------

    public Cursor find(String id) {
        Cursor c = db.query(
                TABLE_NAME,
                PROJECTION,
                WHERE_ID,
                new String[]{id},
                null,
                null,
                null);
        return c;
    }

    public Cursor findByHash(String hash) {
        Cursor c = db.query(
                TABLE_NAME,
                PROJECTION,
                WHERE_HASH,
                new String[]{hash},
                null,
                null,
                null);
        return c;
    }

    public Cursor findByStatus(String status) {
        Cursor c = db.query(
                TABLE_NAME,
                PROJECTION,
                WHERE_STATUS,
                new String[]{status},
                null,
                null,
                null);
        return c;
    }

    public FileObject[] getAllFileObjects() {
        Cursor c = db.query(
                TABLE_NAME,
                PROJECTION,
                null,
                null,
                null,
                null,
                CNAME_TIMESTAMP+" DESC");
        return cursor2files(c);
    }

    public void insert(FileObject r) {
        ContentValues cv = file2ContentValues(r);
        db.insert(TABLE_NAME, null, cv);
    }



    public void update(FileObject r) {
        ContentValues values = file2ContentValues(r);
        db.update(TABLE_NAME, values , WHERE_ID, new String[]{r.id+""});
    }

    public void delete(FileObject r) {
        //Utils.d("delete file " + r.id);
        db.delete(TABLE_NAME, WHERE_ID, new String[]{r.id+""});
    }

    public void deleteAll() {
        Utils.d("delete all from " + TABLE_NAME);
        db.delete(TABLE_NAME, null, null);
    }

    // --------------------------------------------
    // TRANSFORMATION
    // --------------------------------------------



    public static FileObject cursor2file(Cursor c) {
        c.moveToFirst();
        if(c.isAfterLast()) return null;
        FileObject r = new FileObject();
        r.id = c.getInt(CNUM_ID);
        r.timestamp =c.getLong(CNUM_TIMESTAMP);
        r.hash = c.getString(CNUM_HASH);
        r.filename = c.getString(CNUM_FILENAME);
        r.status = c.getString(CNUM_STATUS);
        return r;
    }

    public static FileObject[] cursor2files(Cursor c) {
        c.moveToFirst();
        LinkedList<FileObject> files = new LinkedList<FileObject>();
        while(!c.isAfterLast()){
            FileObject r = new FileObject();
            r.id = c.getInt(CNUM_ID);
            r.timestamp =c.getLong(CNUM_TIMESTAMP);
            r.hash = c.getString(CNUM_HASH);
            r.filename = c.getString(CNUM_FILENAME);
            r.status = c.getString(CNUM_STATUS);
            files.add(r);
            c.moveToNext();
        }
        return files.toArray(new FileObject[0]);
    }

    private static ContentValues file2ContentValues(FileObject r) {
        ContentValues cv = new ContentValues();
        cv.put(CNAME_TIMESTAMP, r.timestamp);
        cv.put(CNAME_HASH, r.hash);
        cv.put(CNAME_FILENAME, r.filename);
        cv.put(CNAME_STATUS, r.status);
        return cv;
    }



}

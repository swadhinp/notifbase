package hplabs.utaustin.cs.swadhin.notifbase.service;

import android.util.Log;

import java.util.ArrayList;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.SilenceDetector;
import be.tarsos.dsp.beatroot.BeatRootOnsetEventHandler;
import be.tarsos.dsp.mfcc.MFCC;
import be.tarsos.dsp.onsets.ComplexOnsetDetector;
import be.tarsos.dsp.onsets.OnsetHandler;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;


/**
 * Created by swadhin on 7/6/15.
 */
public class AudioObserver {

    public final static int sample_rate = 22050; //22.05 kHz
    public final static int audio_buffer_size = 1024;
    public final static int audio_buffer_overlap = 0;
    public final static int audio_buffer_beat_overlap = 256;

    public final static int window_size = 15; //15 seconds

    private float pitchInHz = -1;
    private double rmsValue = -1.0;
    private double splValue = -1.0;
    private double salValue = -1.0;
    private double beatValue = -1.0;


    private PitchDetectionHandler pdh = null;
    public AudioDispatcher dispatcher = null;
    private AudioProcessor audiopitch = null;
    private AudioProcessor audiorms = null;
    private AudioProcessor audiospl = null;
    private AudioProcessor audiomfcc = null;
    private AudioProcessor zerocrossr = null;

    private ComplexOnsetDetector audiodetector = null;
    private ComplexOnsetDetector beatdetector = null;
    private String audioString = "";

    public  static ArrayList<String> rms_list = new ArrayList<String>();
    public  static ArrayList<String> pitch_list = new ArrayList<String>();
    public  static ArrayList<String> spl_list = new ArrayList<String>();
    public  static ArrayList<String> mfcc_list = new ArrayList<String>();

    public static String audio_pitch_value = "No";
    public static String audio_decibel = "50 dB";


    public AudioObserver(){}

    public void init (){


        //dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(sample_rate, audio_buffer_size, audio_buffer_overlap);

        //Pitch Extraction
        pdh = new PitchDetectionHandler() {
            @Override
            public void handlePitch(PitchDetectionResult result,AudioEvent e) {
                pitchInHz = result.getPitch();
                audioString = " pitch : " + Float.toString(pitchInHz);

                if(pitchInHz > -1) {
                    audio_pitch_value = "Yes ~ " + Float.toString(pitchInHz);
                }

                pitch_list.add(Float.toString(pitchInHz));

                Log.d("XXX", audioString);
            }
        };

        audiopitch = new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN, sample_rate, audio_buffer_size, pdh);
        dispatcher.addAudioProcessor(audiopitch);

        //RMS Extraction
        audiorms = new AudioProcessor() {
            @Override
            public void processingFinished() {
            }

            @Override
            public boolean process(AudioEvent audioEvent) {
                //System.out.println(audioEvent.getTimeStamp() + "," + audioEvent.getRMS());
                rmsValue = audioEvent.getRMS();
                audioString = " rms : " + Double.toString(rmsValue);
                rms_list.add(Double.toString(rmsValue));

                Log.d("XXX", audioString);
                return true;
            }
        };

        dispatcher.addAudioProcessor(audiorms);

        //Sound Pressure Extraction
        final SilenceDetector silenceDetecor = new SilenceDetector();
        dispatcher.addAudioProcessor(silenceDetecor);
        audiospl = new AudioProcessor() {
            @Override
            public void processingFinished() {
            }

            @Override
            public boolean process(AudioEvent audioEvent) {
                //System.out.println(audioEvent.getTimeStamp() + "," + silenceDetecor.currentSPL());
                splValue = silenceDetecor.currentSPL();
                audioString = " spl : " + Double.toString(splValue);
                Log.d("XXX", audioString);
                audio_decibel = Double.toString(splValue) + " dB";
                spl_list.add(Double.toString(splValue));
                return true;
            }
        };
        dispatcher.addAudioProcessor(audiospl);

        //Onset Extractor
        audiodetector = new ComplexOnsetDetector(audio_buffer_size,0.7,0.1);
        audiodetector.setHandler(new OnsetHandler() {
            @Override
            public void handleOnset(double time, double salience) {
                //System.out.println(time + "," + salience);
                salValue = salience;
                audioString = " sal : " + Double.toString(salValue);
                Log.d("XXX", audioString);
            }
        });
        dispatcher.addAudioProcessor(audiodetector);


        beatdetector = new ComplexOnsetDetector(audio_buffer_size);
        BeatRootOnsetEventHandler handler = new BeatRootOnsetEventHandler();
        handler.trackBeats(new OnsetHandler() {
            @Override
            public void handleOnset(double v, double salience) {
                beatValue = salience;
                audioString = " beat : " + Double.toString(beatValue);
                Log.d("XXX", audioString);
            }
        });
        beatdetector.setHandler(handler);
        dispatcher.addAudioProcessor(beatdetector);

        audiomfcc = new MFCC(audio_buffer_size,sample_rate){
            @Override
            public boolean process(AudioEvent audioEvent) {
                float[] audioFloatBuffer = audioEvent.getFloatBuffer().clone();

                // Magnitude Spectrum
                float bin[] = magnitudeSpectrum(audioFloatBuffer);
                // get Mel Filterbank
                float fbank[] = melFilter(bin, this.getCenterFrequencies());
                // Non-linear transformation
                float f[] = nonLinearTransformation(fbank);
                // Cepstral coefficients
                float[] mfcc = cepCoefficients(f);

                for(float fl: mfcc) {
                    audioString = " mfcc : " + Float.toString(fl);
                    mfcc_list.add(Float.toString(fl));
                    //Log.d("XXX", audioString);
                }
                return true;
            }

            @Override
            public void processingFinished() {

            }

        };
        dispatcher.addAudioProcessor(audiomfcc);


        //zerocrossr = new ZeroCrossingRateProcessor();
        //dispatcher.addAudioProcessor(zerocrossr);

        //dispatcher.run();


    }

    public AudioDispatcher getAudioDispatcher(){
        return dispatcher;
    }
    public void setAudioDispatcher(AudioDispatcher aud){
        dispatcher =aud;
    }


    public PitchDetectionHandler getPdh(){
        return pdh;
    }

    public float getPitch(){
        return pitchInHz;
    }

    public double getRMS(){ return rmsValue;}
    public double getSPL(){ return splValue;}
    public double getSal(){ return salValue;}
    public double getBeat(){ return beatValue;}


    public AudioProcessor getPitchProcessor(){
        return audiopitch;
    }

    public AudioProcessor getRMSProcessor(){
        return audiorms;
    }

    public AudioProcessor getSplProcessor(){
        return audiospl;
    }

    public  ComplexOnsetDetector getAudioDetector(){
        return audiodetector;
    }


    public  ComplexOnsetDetector getBeatDetector(){
        return beatdetector;
    }

    public String getAudioString() {
        return audioString;
    }

}

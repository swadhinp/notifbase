package hplabs.utaustin.cs.swadhin.notifbase.service;

/**
 * Created by swadhin on 7/7/15.
 */
public class CalendarEvent implements Comparable<CalendarEvent>{

    private String title;
    private String begin, end;
    private boolean allDay;


    public CalendarEvent(String title, String begin, String end) {
        setTitle(title);
        setBegin(begin);
        setEnd(end);
        //setAllDay(allDay);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    @Override
    public String toString(){
        return getTitle() + "|" + getEnd();
    }

    @Override
    public int compareTo(CalendarEvent other) {
        // -1 = less, 0 = equal, 1 = greater
        return getBegin().compareTo(other.begin);
    }

}